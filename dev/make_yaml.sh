#!/bin/bash
############################################################################################
#    $Date: 2021-12-18 11:38:15 +0100 (Sa, 18. Dez 2021) $
#    $Revision: 1618 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/dev/make_yaml.sh $
#    $Id: make_yaml.sh 1618 2021-12-18 10:38:15Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
namespace="default"
docker_registry="docker.registry:5000"
#equivalent zu source
. ./env.sh
datum=(`date '+%Y%m%d'`)
revision=(`date '+%H%M%S'`)
#revision=$(svn info | grep "Revision" | awk '{print $2}')
tag=$(cat ${image}.tag)
# Aktualisieren der Sourcen
git pull --ff-only
# yaml updaten
for val in ${namespace}; do
	echo "Namespace: ${val} "
	#Namespacespezifisch
	. ../namespace/${val}_env.sh	
	cat ${image}*.yaml > /tmp/${image}.tmp_1
	sed 's/\${image}/'"${image}"'/g' /tmp/${image}.tmp_1 > /tmp/${image}.tmp_2
	sed 's/\${tag}/'"${tag}"'/g' /tmp/${image}.tmp_2 > /tmp/${image}.tmp_3
	sed 's/\${secretName}/'"${secretName}"'/g' /tmp/${image}.tmp_3 > /tmp/${image}.tmp_4
	sed 's/\${host}/'"${host}"'/g' /tmp/${image}.tmp_4 > /tmp/${image}.tmp_5
	sed 's/\${namespace}/'"${val}"'/g' /tmp/${image}.tmp_5 > /tmp/${image}.tmp_6
	sed 's/\${namespace_comment}/'"${namespace_comment}"'/g' /tmp/${image}.tmp_6 > /tmp/${image}.tmp_7
	sed 's/\${cluster_issuer}/'"${cluster_issuer}"'/g' /tmp/${image}.tmp_7 > /tmp/${image}.tmp_8
	sed 's/\${docker_registry}/'"${docker_registry}"'/g' /tmp/${image}.tmp_8 > /tmp/${image}.yaml
	#
	# Nun kann das yaml verwendet werden
	#
	ansible pc1 -m copy -a "src="/tmp/${image}.yaml" dest=./yaml/"${image}_${val}.yaml
	# Installieren
    	ansible pc1 -m shell -a "microk8s kubectl apply -f ./yaml/"${image}_${val}.yaml" --namespace="${val}
done
rm -f /tmp/${image}*.*
#
