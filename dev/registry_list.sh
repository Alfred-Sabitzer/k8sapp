#!/bin/bash
############################################################################################
#    $Date: 2021-11-25 21:57:57 +0100 (Do, 25. Nov 2021) $
#    $Revision: 1348 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/dev/registry_list.sh $
#    $Id: registry_list.sh 1348 2021-11-25 20:57:57Z alfred $
#
# Manipulieren der microk8s.registry
# https://gist.github.com/Kevinrob/4c7f1e5dbf6ce4e94d6ba2bfeff37aeb
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.registry:5000"

repositories=$(curl -s ${docker_registry}/v2/_catalog)
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo $repo
  tags=$(curl -s "http://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in $tags; do
    echo "      "$repo:$tag
  done
done
#
