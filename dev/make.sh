#!/bin/bash
############################################################################################
#    $Date: 2021-12-18 11:38:15 +0100 (Sa, 18. Dez 2021) $
#    $Revision: 1618 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/dev/make.sh $
#    $Id: make.sh 1618 2021-12-18 10:38:15Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -s expand_aliases # Benutzen von aliasen im Skript
shopt -o -s nounset #-No Variables without definition
namespace="default"
docker_registry="docker.registry:5000"
#equivalent zu source
. ./env.sh
datum=(`date '+%Y%m%d'`)
revision=(`date '+%H%M%S'`)
#revision=$(svn info | grep "Revision" | awk '{print $2}')
tag="${datum}-${revision}"
echo ${tag} > ${image}.tag # Sicherstellen eines stabilen Image-tags
# Aktualisieren der Sourcen
. ${HOME}/.bash_aliases
doDocker=${BASH_ALIASES[mkd]}
doYaml=${BASH_ALIASES[mky]}
${doDocker}
${doYaml}
#
