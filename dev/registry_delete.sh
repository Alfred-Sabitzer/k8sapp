#!/bin/bash
############################################################################################
#    $Date: 2021-11-25 21:57:57 +0100 (Do, 25. Nov 2021) $
#    $Revision: 1348 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/dev/registry_delete.sh $
#    $Id: registry_delete.sh 1348 2021-11-25 20:57:57Z alfred $
#
# Manipulieren der microk8s.registry
# https://gist.github.com/Kevinrob/4c7f1e5dbf6ce4e94d6ba2bfeff37aeb
#
# webdefault:20211123-1301
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.registry:5000"

# Prüfung ob gesetzt, sonst liefert die Shell hier eine Fehlermeldung
string="${1}"
#echo "${string}"
prefix=":"
suffix=""
xrepo=${string%"$prefix"*}
#echo "${xrepo}"
xtag=${string#"$xrepo$prefix"}
#echo "${xtag}"
xtag=${xtag%"$suffix"*}
#echo "${xtag}"

tags=$(curl -sSL "http://${docker_registry}/v2/${xrepo}/tags/list" | jq -r '.tags[]')
for tag in $tags; do
	if [ "${tag} " = "${xtag} " ] ;
	then
		echo "Lösche      "$xrepo:$tag
		curl -v -k -X DELETE "http://${docker_registry}/v2/${xrepo}/manifests/$(
		      curl -k -s -I \
		          -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
		          "http://${docker_registry}/v2/${xrepo}/manifests/${xtag}" \
		      | awk '$1 == "Docker-Content-Digest:" { print $2 }' \
		      | tr -d $'\r' \
		    )"
	fi
done
#
