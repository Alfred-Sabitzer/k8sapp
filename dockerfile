#
# Testimage um Docker im Docker zu zeigen.
# Das ist nur ein Test
#
FROM docker:20.10.12-dind-alpine3.15

LABEL maintainer="microk8s.raspberry@slainte.at"
LABEL Description="CI/CD für MicroK8s"

RUN addgroup docker
RUN mkdir -p /var/lib/docker 
RUN mkdir -p /var/run
RUN touch /var/run/docker.sock
RUN chown root:docker /var/run/docker.sock

ENTRYPOINT ["/usr/local/bin/dockerd-entrypoint.sh"]
