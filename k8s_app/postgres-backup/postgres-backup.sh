#!/bin/sh
############################################################################################
# Durchführen des Postgres Backups
############################################################################################

echo $*
date
fdate=$(date '+%Y-%m-%d-%H:%M:%S')
PGSERVICE=${1}
PGDB=${2}
PGACTION=${3}
if [ "${PGDB} " = " " ] ;
  then
    echo " Usage: args: ["pgadmin-svc.admin.svc","pgadmin","backup"]"
    echo " args1: Namespace/Servicename des Datenbank-Services"
    echo " args2: Name der Datenbank"
    echo " args3: Durchzuführende Aktivität"
    exit 1
  fi

PG_DB_USER=$(cat /pgadmin/username)
PG_DB_PASSWORD=$(cat /pgadmin/password)

echo ${PGSERVICE} ${PGDB} ${PGACTION}
ipaddr=$(ping -c1 -q -w 1 ${PGSERVICE} | grep -i ${PGSERVICE})
echo ${ipaddr}
pf="("
sf=")"
host=${ipaddr#*"$pf"}
host=${host%"$sf"*}
echo ${host}

pg_dump --no-owner --dbname=postgresql://${PG_DB_USER}:${PG_DB_PASSWORD}@${host}:5432/${PGDB} | gzip -9 > /postgresbackupdir/${PGSERVICE}_${fdate}.sql.gz

ls -lisa /postgresbackupdir/*.*