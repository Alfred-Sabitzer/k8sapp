#!/bin/bash
############################################################################################
# Bauen und deployen
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull --ff-only
image="postgres-backup"
postgres_backupdir="/tmp/postgres_backupdir"
sudo rm -f ./${image}_latest_container.tar.gz
sudo rm -Rf ./${image}_latest_container
docker build --no-cache --force-rm . -t ${image}:latest
#docker save ${image}:latest | gzip > ${image}_latest.tar.gz
docker kill $(docker ps | grep -i ${image}:latest | awk '{print $1 }') 
docker container rm -f $(docker container ls -a | grep -i ${image}_latest | awk '{print $1 }') 
docker run -d --name ${image}_latest -v ${postgres_backupdir}:/postgres_backupdir ${image}:latest
sleep 10
docker ps
docker export $(docker ps | grep -i ${image}:latest | awk '{print $1 }') | gzip >  ${image}_latest_container.tar.gz
echo "docker exec -u 0 -it ${image}_latest sh -c \"clear; (bash || ash || sh) \""
#

