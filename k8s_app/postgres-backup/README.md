# Backup der Postgres DB

Die Datenbanken werden mit pg_dump exportiert und gezippt abgelegt.

## Ablauf
    echo " Usage: args: ["pgadmin-svc.admin.svc","pgadmin","backup"]"
    echo " args1: Namespace/Servicename des Datenbank-Services"
    echo " args2: Name der Datenbank"
    echo " args3: Durchzuführende Aktivität"

## Support
Es gibt keinen Support für diese Software. Jede Software hat experimentellen Charakter. Es wird dringend davon abgeraten, diese Software produktiv zu verwenden.

## Lizenz
GPL https://www.gnu.org/licenses/gpl-3.0.html
