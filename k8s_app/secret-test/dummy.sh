#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-10-31 15:53:33 +0100 (So, 31. Okt 2021) $"
echo "#    $Revision: 1008 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/secret-test/dummy.sh $"
echo "#    $Id: dummy.sh 1008 2021-10-31 14:53:33Z alfred $"
echo "#"
echo "# Einfaches Dummy, macht nix "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
while [ true ]
do
	echo "`date` [`hostname`] $Id: dummy.sh 1008 2021-10-31 14:53:33Z alfred $"
	export
	sleep $((300 + RANDOM % 11));
done
echo "Das wird nie passieren"
