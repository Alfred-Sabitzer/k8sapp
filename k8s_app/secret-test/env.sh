#!/bin/bash
############################################################################################
#    $Date: 2021-10-31 15:53:33 +0100 (So, 31. Okt 2021) $
#    $Revision: 1008 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/secret-test/env.sh $
#    $Id: env.sh 1008 2021-10-31 14:53:33Z alfred $
#
# Testen, wie sie k8s mit Secrets verhält
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="secret-test"
export namespace="default"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
