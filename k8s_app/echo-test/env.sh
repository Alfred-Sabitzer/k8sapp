#!/bin/bash
############################################################################################
#    $Date: 2021-11-28 09:47:03 +0100 (So, 28. Nov 2021) $
#    $Revision: 1394 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/echo-test/env.sh $
#    $Id: env.sh 1394 2021-11-28 08:47:03Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="echo-test"
export namespace="default"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
