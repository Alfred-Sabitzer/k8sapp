#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-10-24 12:47:38 +0200 (So, 24. Okt 2021) $"
echo "#    $Revision: 767 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/chrony/chrony.sh $"
echo "#    $Id: chrony.sh 767 2021-10-24 10:47:38Z alfred $"
echo "#"
echo "# Starten des Chrony-Daemons"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
ls -lisa /
cat /etc/chrony/chrony.conf
/usr/sbin/chronyd -f /etc/chrony/chrony.conf -d -s
#
