#!/bin/bash
############################################################################################
#    $Date: 2021-12-02 11:45:26 +0100 (Do, 02. Dez 2021) $
#    $Revision: 1473 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/chrony/env.sh $
#    $Id: env.sh 1473 2021-12-02 10:45:26Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="chrony"
export namespace="admin"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
