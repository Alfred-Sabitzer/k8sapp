#!/bin/bash
############################################################################################
#    $Date: 2021-12-02 20:59:48 +0100 (Do, 02. Dez 2021) $
#    $Revision: 1486 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/webserver/env.sh $
#    $Id: env.sh 1486 2021-12-02 19:59:48Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="webserver"
export namespace="slainte"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
