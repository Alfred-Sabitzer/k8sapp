#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-12-03 21:14:25 +0100 (Fr, 03. Dez 2021) $"
echo "#    $Revision: 1495 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/webserver/start.sh $"
echo "#    $Id: start.sh 1495 2021-12-03 20:14:25Z alfred $"
echo "#"
echo "# Aufbereiten des Containers"
echo "#"
echo "############################################################################################"

# Kopieren der Zertifikate und der Config
cp -v -f /etc/nginx/config/* /etc/nginx/
rm -rf /etc/nginx/conf.d/default.conf || true
rm -rf /usr/local/etc/php-fpm.conf || true
ln -s /etc/php7/php-fpm.conf /usr/local/etc/php-fpm.conf 
cp -v -f /etc/nginx/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
cp -v -f /etc/nginx/php.ini /etc/php7/conf.d/custom.ini
cp -v -f /etc/nginx/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Socket erzeugen
touch /var/run/php/php-fpm.sock
chown -R www-data:www-data /var/run/php
chown www-data:www-data /var/run/php/php-fpm.sock
chmod 766 /var/run/php/php-fpm.sock

# Rechte richtig setzen
chown -R www-data:www-data /etc/php7
chown -R www-data:www-data /etc/nginx

ls -lisaR /etc/nginx/*

# Setzen der Variablen
export JOOMLA_DB_USER=$(cat /joomla_db/username)
export JOOMLA_DB_PASSWORD=$(cat /joomla_db/password)
      
# Prüfen ob Erstinstallation
DIR="/var/www/html"
if [ -d "$DIR" ]
then
	if [ "$(ls -A $DIR)" ]; then
	    echo "$DIR existiert. Setzen der Passwörter"
	else
	    echo "$DIR ist leer. Neuinstallation"
	    cp -R -v -f /html/ $DIR/../
	    tar -xz -f /joomla/*.tar.gz -C $DIR
	fi
else
	echo "Verzeichnis $DIR nicht gefunden."
	exit -1
fi
	
chown -R www-data:www-data $DIR
chown -R www-data:www-data $DIR/*
chown -R www-data:www-data $DIR/*.*

#ls -lisaR $DIR/*

# Starten des Supervisors
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
