#/bin/bash
# Methoden generiert am 2021-12-03T21:30:41.464923
# Generiertes File um die Passwortänderungen durchzuführen
# Name joomla
# Passwort für den Joomla-Schema-User in der Postgres-DB
./do_pg.sh pg_user "slainte" "admin/pgadmin-svc" "am9vbWxh" "N2VWcWJ0dlZ4dnY5YlpMNUhxT2JMMnN2WXFEVGpQZ3JRRTJvRVE1dA=="
# Name pgadmin
# Dieses Passwort ist für den Postgresadmin. Kommt als Environmentvariable in den Container. Dieser User bekommt die Role Superuser.
./do_pg.sh pg_admin "admin" "admin/pgadmin-svc" "cGdhZG1pbg==" "SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA=="
# Name pgadmin
# Dieses Passwort ist für den Postgresadmin. Kommt als Environmentvariable in den Container. Dieser User bekommt die Role Superuser.
./do_pg.sh pg_user "admin" "admin/pgadmin-svc" "cGdhZG1pbg==" "SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA=="
# Name pgmaster
# Verwalten von vielen Datenbanken
./do_pg.sh pg_admin "admin" "admin/pgadmin-svc" "azhzLnBvc3RncmVzLmFsbGdlbWVpbkBzbGFpbnRlLmF0" "RDhsZUhOY1BSelJuU212bGJlMnQ="
# Name pgmaster
# Verwalten von vielen Datenbanken
./do_pg.sh pg_user "admin" "admin/pgadmin-svc" "azhzLnBvc3RncmVzLmFsbGdlbWVpbkBzbGFpbnRlLmF0" "RDhsZUhOY1BSelJuU212bGJlMnQ="
