#!/bin/bash
############################################################################################
#    $Date: 2021-12-03 21:48:50 +0100 (Fr, 03. Dez 2021) $
#    $Revision: 1501 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/mykeepass/do_pg.sh $
#    $Id: do_pg.sh 1501 2021-12-03 20:48:50Z alfred $
#
# Kommunikation mit dem Postgress-Rest-API
#
#./do_pg.sh pg_admin "slainte" "pgadmin-svc" "cGdhZG1pbg==" "SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA=="
#./do_pg.sh pg_user "slainte" "pgadmin-svc" "cGdhZG1pbg==" "SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA=="
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
# Überprüfen der Parameter
if [ "${1}" = "" ] || [ "${2}" = "" ] || [ "${3}" = "" ] || [ "${4}" = "" ]  || [ "${5}" = "" ] ;
then
	echo "Usage: $0 Methode Namespace Service User Passwort "
	echo "eg: $0	pg_user \"slainte\" \"pgadmin-svc\" \"cGdhZG1pbg==\" \"SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA==\" "
	exit -1
fi
shopt -o -s nounset #-No Variables without definition
#
localuser='alfred@slainte.at'
recipient='k8s.postgres.allgemein@slainte.at'
secret=$(mktemp /tmp/do_pg.XXXXXX)
secret2=$(mktemp /tmp/do_pg.XXXXXX)
signature=$(mktemp /tmp/do_pg.XXXXXX)
action=$(mktemp /tmp/do_pg.XXXXXX)
action2=$(mktemp /tmp/do_pg.XXXXXX)
xdate=$(mktemp /tmp/do_pg.XXXXXX)
xdate2=$(mktemp /tmp/do_pg.XXXXXX)
message=$(mktemp /tmp/do_pg.XXXXXX)
result=$(mktemp /tmp/do_pg.XXXXXX)
signature1=$(mktemp /tmp/do_pg.XXXXXX)
signature2=$(mktemp /tmp/do_pg.XXXXXX)
secret1=$(mktemp /tmp/do_pg.XXXXXX)
secret2=$(mktemp /tmp/do_pg.XXXXXX)
#
communicate ()
{
echo $(date +"%Y-%m-%d %H:%M:%S %s") > ${xdate}
cat <<EOF > ${action}
psql
EOF

gpg --encrypt -a --local-user ${localuser} --recipient ${recipient} --output ${xdate}.asc ${xdate}
gpg --encrypt -a --local-user ${localuser} --recipient ${recipient} --output ${action}.asc ${action} 
gpg --encrypt -a --local-user ${localuser} --recipient ${recipient} --output ${secret}.asc ${secret} 
gpg --detach-sign -a --local-user ${localuser} --output ${signature}.sig ${secret}.asc
gpg --verify ${signature}.sig ${secret}.asc

#
# Erzeugen der Nachricht
#
cat <<EOF > ${message}
<?xml version="1.0" standalone="yes"?>
<message>
<time_stamp>$(cat ${xdate}.asc)
</time_stamp>
<action>$(cat ${action}.asc)
</action>
<signature>$(cat ${signature}.sig)
</signature>
<secret>$(cat ${secret}.asc)
</secret>
</message>
EOF

#
# Übertragen der Nachricht
#
cmd="kubectl -n ${1} port-forward service/${2} 9999:443"
$cmd &
pid=$!
sleep 5
curl -k --insecure --data-binary @${message} -H "Content-Type: application/xml" https://localhost:9999/RestAPI/ > ${result}
echo "Ergebnis in ${result}"
kill ${pid}
}

pg_admin ()
{
#echo "pg_admin "${namespace}" "${service}" "${user}" "${passwort}" "${serviceIP}" ;;

cat <<EOF > ${secret}
-- Anlegen des Users
select create_superuser('${3}','${4}');
select set_password('${3}','${4}');
-- Anlegen der Datenbank
select create_database('${3}','${3}');
select grant_database_privileges('ALL','${3}','${3}');
select * from pg_catalog.pg_database;
select * from pg_catalog.pg_user;
EOF
communicate $1 $2 $5
}

pg_user ()
{
#echo  "pg_user "${namespace}" "${service}" "${user}" "${passwort}" "${serviceIP}";;"
cat <<EOF > ${secret}
-- Anlegen des Users
select create_user('${3}','$4}');
select set_password('${3}','${4}');
-- Anlegen der Datenbank
select create_database('${3}','${3}');
select grant_database_privileges('ALL','${3}','${3}');
select * from pg_catalog.pg_database;
select * from pg_catalog.pg_user;
EOF
communicate $1 $2 $5
}
#
methode=$1
namespace_source=$2
service=$3
user=$(echo $4 | base64 --decode)
passwort=$(echo $5 | base64 --decode)
#./do_pg.sh pg_user "slainte" "admin/pgadmin-svc" "am9vbWxh" "N2VWcWJ0dlZ4dnY5YlpMNUhxT2JMMnN2WXFEVGpQZ3JRRTJvRVE1dA=="
namespace=${service%"/"*}
service=${service#*"/"}
#echo ${methode} ${namespace} ${service} ${user} ${passwort}

serviceIP=$(kubectl get -n ${namespace} service | grep -i ${service} | awk '{print $3 }')
if [ "${serviceIP} " = " " ] ;
then
	echo "FEHLER: ${service} im Namespace ${namespace} kann nicht gefunden werden! ${serviceIP}"
	exit -1
fi

case ${methode} in
    pg_admin)
        pg_admin "${namespace}" "${service}" "${user}" "${passwort}" "${serviceIP}" ;;
    pg_user)
        pg_user "${namespace}" "${service}" "${user}" "${passwort}" "${serviceIP}";;
    *) ;;
esac

decrypt()
{
  FILE=$1
  MAGIC_WORD=$2
  OUTFILE=$3
  
  grep -Ezo '(?'${MAGIC_WORD}')[^<]+' ${FILE} > ${OUTFILE}
  sed -i 's,'${MAGIC_WORD}',,g' ${OUTFILE}
  sed -i '/-----END PGP MESSAGE-----/q' ${OUTFILE}
  rm -f ${OUTFILE}.txt
  gpg --decrypt --output ${OUTFILE}.txt ${OUTFILE}
}

grep -Ezo '(?<signature>)[^<]+' "${result}" > ${signature1}
sed 's,<signature>,,g' ${signature1} > ${signature2}
sed '/-----END PGP SIGNATURE-----/q' ${signature2} > ${signature1}

grep -Ezo '(?<secret>)[^<]+' "${result}" > ${secret1}
sed 's,<secret>,,g' ${secret1} > ${secret2}
sed '/-----END PGP MESSAGE-----/q' ${secret2} > ${secret1}

gpg --verify ${signature1} ${secret1}
if [ $? -eq 0 ]; 
then 
    echo "Gültige Signatur" 
else 
    echo "Ungültige Signatur"
    exit -1
fi

decrypt "${result}" "<time_stamp>" ${xdate2}
decrypt "${result}" "<action>" ${action2}
decrypt "${result}" "<secret>" ${secret2}

echo $'\n**** Date ****\n'
cat ${xdate2}.txt
echo $'\n**** Status ****\n'
cat ${action2}.txt
echo $'\n**** Meldung ****\n'
cat ${secret2}.txt
echo $'\n****\n'

rm -f /tmp/do_pg.*
#

