#!/bin/bash
############################################################################################
#    $Date: 2021-12-02 14:20:42 +0100 (Do, 02. Dez 2021) $
#    $Revision: 1481 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgmaster/env.sh $
#    $Id: env.sh 1481 2021-12-02 13:20:42Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="pgmaster"
export namespace="admin"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
