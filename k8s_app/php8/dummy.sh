#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-12-12 15:12:36 +0100 (So, 12. Dez 2021) $"
echo "#    $Revision: 1591 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/php8/dummy.sh $"
echo "#    $Id: dummy.sh 1591 2021-12-12 14:12:36Z alfred $"
echo "#"
echo "# Einfaches Dummy, macht nix "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
while [ true ]
do
	echo "`date` [`hostname`] $Id: dummy.sh 1591 2021-12-12 14:12:36Z alfred $"
	sleep $((300 + RANDOM % 11));
done
echo "Das wird nie passieren"
