#!/bin/bash
############################################################################################
#    $Date: 2021-12-12 15:12:36 +0100 (So, 12. Dez 2021) $
#    $Revision: 1591 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/php8/env.sh $
#    $Id: env.sh 1591 2021-12-12 14:12:36Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="php8"
export namespace="default"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
