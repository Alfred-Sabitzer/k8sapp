#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/start.sh $"
echo "#    $Id: start.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Konfiguration des pgpAdmins "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.

function cleanup()
{
    echo "Exit signal received"
    su postgres -c 'pg_ctl stop'
    echo "Bye Bye"
}

trap cleanup TERM

source set_env.sh
export
/make_db.sh
/pgpAdmin.sh
/lighttpd.sh
#/dummy.sh
