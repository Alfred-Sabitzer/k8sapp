#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/dummy.sh $"
echo "#    $Id: dummy.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Einfaches Dummy, macht nix "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
while [ true ]
do
	echo "`date` [`hostname`] $Id: dummy.sh 1427 2021-11-28 19:15:12Z alfred $"
	sleep $((300 + RANDOM % 11));
done
echo "Das wird nie passieren"
