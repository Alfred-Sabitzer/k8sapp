#!/bin/bash
############################################################################################
#    $Date: 2021-12-02 11:56:20 +0100 (Do, 02. Dez 2021) $
#    $Revision: 1477 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/env.sh $
#    $Id: env.sh 1477 2021-12-02 10:56:20Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="pgadmin"
export namespace="admin"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
