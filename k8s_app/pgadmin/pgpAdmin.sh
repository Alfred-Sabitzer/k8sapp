#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/pgpAdmin.sh $"
echo "#    $Id: pgpAdmin.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Konfiguration des pgpAdmins "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
echo ${HOSTNAME}
sed 's,##HOSTNAME##,'${HOSTNAME}',g' ${PHPADMIN_INC} > /usr/share/webapps/phpPgAdmin-7.13.0/conf/config.inc.php
chmod -R 777 /usr/share/webapps/
chown -R lighttpd:wheel /usr/share/webapps/
#
# Nun kommt der pgp-Teil
#
for filename in /pgp/*.asc; do
	gpg --import ${filename}
done
gpg --import-ownertrust < /pgp/trust.txt 
gpg --list-keys --keyid-format SHORT
#Support für sudo
echo 'lighttpd ALL=(ALL) NOPASSWD: /usr/bin/gpg *' >> /etc/sudoers
echo 'lighttpd ALL=(ALL) NOPASSWD: /usr/share/webapps/RestAPI/mygpg.sh *' >> /etc/sudoers

#
echo "-"
echo "- Create Schema "
echo "-"
cat <<EOF > /tmp/create_pgpAdmin.sql

CREATE TABLE IF NOT EXISTS pgadmin_message (
	id serial4 NOT NULL, -- id
	zeitstempel text NOT NULL, -- Zeitstempel aus der Message
	signature text NOT NULL, -- Signature aus der Message
	to_do text NOT NULL, -- Action aus der Message
	message text NOT NULL, -- Nachricht
	log text NULL, -- Postgres Output
	status varchar NOT NULL, -- Status der Nachricht
	datum timestamptz NOT NULL DEFAULT now(), -- Zeitstempel der Verarbeitung
	sender text NOT NULL, -- Sender der Nachricht
	CONSTRAINT pgadmin_message_pk PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN pgadmin_message.id IS 'id';
COMMENT ON COLUMN pgadmin_message.zeitstempel IS 'Zeitstempel aus der Message';
COMMENT ON COLUMN pgadmin_message.signature IS 'Signature aus der Message';
COMMENT ON COLUMN pgadmin_message.to_do IS 'Action aus der Message';
COMMENT ON COLUMN pgadmin_message.message IS 'Nachricht';
COMMENT ON COLUMN pgadmin_message.log IS 'postgres output';
COMMENT ON COLUMN pgadmin_message.status IS 'Status der Nachricht';
COMMENT ON COLUMN pgadmin_message.datum IS 'Zeitstempel der Verarbeitung';
COMMENT ON COLUMN pgadmin_message.sender IS 'Sender der Nachricht';

-- Permissions

ALTER TABLE pgadmin_message OWNER TO postgres;
GRANT ALL ON TABLE pgadmin_message TO postgres;

-- Sequence

CREATE SEQUENCE IF NOT EXISTS pgadmin_message_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

-- Permissions

ALTER SEQUENCE pgadmin_message_id_seq OWNER TO postgres;
GRANT ALL ON SEQUENCE pgadmin_message_id_seq TO postgres;
EOF
su postgres -c 'psql -f /tmp/create_pgpAdmin.sql'
rm -f /tmp/create_pgpAdmin.sql
#
