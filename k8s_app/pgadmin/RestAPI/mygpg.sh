#!/bin/sh
############################################################################################
#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $
#    $Revision: 1427 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/RestAPI/mygpg.sh $
#    $Id: mygpg.sh 1427 2021-11-28 19:15:12Z alfred $
#
# Commandos als Root
#
############################################################################################
echo $1
indir=$1
#ls -lisa ${indir} > /tmp/mgpg.list
echo " "
echo "Action ${indir}"
for filename in ${indir} ; do
	echo "----------------> ${filename}"
        chown lighttpd:wheel ${filename}
        chmod 666 ${filename}
done
echo "Fertig"
#
