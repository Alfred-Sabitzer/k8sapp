/* ############################################################################################
 * #    $Date: 2021-12-01 18:05:02 +0100 (Mi, 01. Dez 2021) $
 * #    $Revision: 1453 $
 * #    $Author: alfred $
 * #    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/RestAPI/init.sql $
 * #    $Id: init.sql 1453 2021-12-01 17:05:02Z alfred $
 * #
 * # definieren allgemein verfügbarer Methoden in der Postgres DB, muß als postgres gemacht werden
 * #
 * ############################################################################################

schemaname	tablename	tableowner	tablespace
pg_catalog	pg_statistic	postgres	
pg_catalog	pg_type	postgres	
pg_catalog	pg_foreign_table	postgres	
pg_catalog	pg_authid	postgres	pg_global
pg_catalog	pg_statistic_ext_data	postgres	
pg_catalog	pg_user_mapping	postgres	
pg_catalog	pg_subscription	postgres	pg_global
pg_catalog	pg_attribute	postgres	
pg_catalog	pg_proc	postgres	
pg_catalog	pg_class	postgres	
pg_catalog	pg_attrdef	postgres	
pg_catalog	pg_constraint	postgres	
pg_catalog	pg_inherits	postgres	
pg_catalog	pg_index	postgres	
pg_catalog	pg_operator	postgres	
pg_catalog	pg_opfamily	postgres	
pg_catalog	pg_opclass	postgres	
pg_catalog	pg_am	postgres	
pg_catalog	pg_amop	postgres	
pg_catalog	pg_amproc	postgres	
pg_catalog	pg_language	postgres	
pg_catalog	pg_largeobject_metadata	postgres	
pg_catalog	pg_aggregate	postgres	
pg_catalog	pg_statistic_ext	postgres	
pg_catalog	pg_rewrite	postgres	
pg_catalog	pg_trigger	postgres	
pg_catalog	pg_event_trigger	postgres	
pg_catalog	pg_description	postgres	
pg_catalog	pg_cast	postgres	
pg_catalog	pg_enum	postgres	
pg_catalog	pg_namespace	postgres	
pg_catalog	pg_conversion	postgres	
pg_catalog	pg_depend	postgres	
pg_catalog	pg_database	postgres	pg_global
pg_catalog	pg_db_role_setting	postgres	pg_global
pg_catalog	pg_tablespace	postgres	pg_global
pg_catalog	pg_auth_members	postgres	pg_global
pg_catalog	pg_shdepend	postgres	pg_global
pg_catalog	pg_shdescription	postgres	pg_global
pg_catalog	pg_ts_config	postgres	
pg_catalog	pg_ts_config_map	postgres	
pg_catalog	pg_ts_dict	postgres	
pg_catalog	pg_ts_parser	postgres	
pg_catalog	pg_ts_template	postgres	
pg_catalog	pg_extension	postgres	
pg_catalog	pg_foreign_data_wrapper	postgres	
pg_catalog	pg_foreign_server	postgres	
pg_catalog	pg_policy	postgres	
pg_catalog	pg_replication_origin	postgres	pg_global
pg_catalog	pg_default_acl	postgres	
pg_catalog	pg_init_privs	postgres	
pg_catalog	pg_seclabel	postgres	
pg_catalog	pg_shseclabel	postgres	pg_global
pg_catalog	pg_collation	postgres	
pg_catalog	pg_partitioned_table	postgres	
pg_catalog	pg_range	postgres	
pg_catalog	pg_transform	postgres	
pg_catalog	pg_sequence	postgres	
pg_catalog	pg_publication	postgres	
pg_catalog	pg_publication_rel	postgres	
pg_catalog	pg_subscription_rel	postgres	
pg_catalog	pg_largeobject	postgres	
information_schema	sql_parts	postgres	
information_schema	sql_implementation_info	postgres	
information_schema	sql_features	postgres	
information_schema	sql_sizing	postgres	
  
*/

CREATE EXTENSION dblink;

CREATE OR REPLACE FUNCTION create_database(theDatabase text, theOwner text)
RETURNS void AS
$BODY$
DECLARE
BEGIN
  IF NOT EXISTS (
    SELECT FROM pg_catalog.pg_database
    WHERE datname = theDatabase) THEN
    BEGIN
      PERFORM dblink_exec('dbname=' || current_database()  -- current db
                        , concat('CREATE DATABASE ',theDatabase,' WITH OWNER ', theOwner));
      RAISE NOTICE 'Database % created', theDatabase;
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
    END;
  END IF;
END;
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION create_user(theUsername text,thePassword text)
RETURNS void AS
$BODY$
DECLARE
BEGIN
  IF NOT EXISTS (
    SELECT FROM pg_catalog.pg_user
    WHERE usename = theUsername) THEN
    BEGIN
      EXECUTE format(
        'CREATE ROLE %I WITH LOGIN ENCRYPTED PASSWORD %L',
        theUsername,
        thePassword
      );

      RAISE NOTICE 'user ''%'' created', theUsername;
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
    END;
  END IF;
END;
$BODY$
LANGUAGE 'plpgsql';

/*

https://www.postgresql.org/docs/current/sql-createrole.html

CREATE ROLE name [ [ WITH ] option [ ... ] ]

where option can be:

      SUPERUSER | NOSUPERUSER
    | CREATEDB | NOCREATEDB
    | CREATEROLE | NOCREATEROLE
    | INHERIT | NOINHERIT
    | LOGIN | NOLOGIN
    | REPLICATION | NOREPLICATION
    | BYPASSRLS | NOBYPASSRLS
    | CONNECTION LIMIT connlimit
    | [ ENCRYPTED ] PASSWORD 'password' | PASSWORD NULL
    | VALID UNTIL 'timestamp'
    | IN ROLE role_name [, ...]
    | IN GROUP role_name [, ...]
    | ROLE role_name [, ...]
    | ADMIN role_name [, ...]
    | USER role_name [, ...]
    | SYSID uid

*/

CREATE OR REPLACE FUNCTION create_superuser(theUsername text,thePassword text)
RETURNS void AS
$BODY$
DECLARE
BEGIN
  IF NOT EXISTS (
    SELECT FROM pg_catalog.pg_user
    WHERE usename = theUsername) THEN
    BEGIN
      EXECUTE format(
        'CREATE ROLE %I WITH LOGIN SUPERUSER CREATEDB CREATEROLE INHERIT REPLICATION ENCRYPTED PASSWORD %L',
        theUsername,
        thePassword
      );

      RAISE NOTICE 'user ''%'' created', theUsername;
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
    END;
  END IF;
END;
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION set_password(theUsername text,thePassword text)
RETURNS void AS
$BODY$
DECLARE
BEGIN
  IF EXISTS (
    SELECT FROM pg_catalog.pg_user
    WHERE usename = theUsername) THEN
    BEGIN
      EXECUTE format(
        'ALTER USER %I WITH ENCRYPTED PASSWORD %L',
        theUsername,
        thePassword
      );    
      RAISE NOTICE 'Password changed!';
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
    END;
  END IF;
END;
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION grant_database_privileges(thePrivilege text, theDatabase text, theUsername text)
RETURNS void AS
$BODY$
DECLARE
BEGIN
    BEGIN
      EXECUTE format(
        'GRANT %I ON DATABASE %D TO %L',
        thePrivilege,
        theDatabase,
        theUsername
      );    
      RAISE NOTICE 'Privileges granted!';
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;
    END;
END;
$BODY$
LANGUAGE 'plpgsql';

