<?php
/* 
############################################################################################
#    $Date: 2021-12-01 18:16:30 +0100 (Mi, 01. Dez 2021) $
#    $Revision: 1454 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/RestAPI/index.php $
#    $Id: index.php 1454 2021-12-01 17:16:30Z alfred $
#
# Bauen und deployen
#
############################################################################################
*/

header("Content-Type: application/xml");
header("Acess-Control-Allow-Origin: *");
header("Acess-Control-Allow-Methods: POST");
header("Acess-Control-Allow-Headers: Acess-Control-Allow-Headers,Content-Type,Acess-Control-Allow-Method");

// Only allow POST requests
if (strtoupper($_SERVER['REQUEST_METHOD']) != 'POST') {
  throw new Exception('Only POST requests are allowed');
}

// Make sure Content-Type is application/json 
$content_type = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
if (stripos($content_type, 'application/xml') === false) {
  throw new Exception('Content-Type must be application/xml');
}

// Vorbereiten der Antwort

echo '<?xml version="1.0" standalone="yes"?>';
echo '<message>';
echo '<log>';

// Read the input stream
$rawdata = file_get_contents("php://input");

// Decode the XML object
libxml_use_internal_errors(true);
$data = new DOMDocument('1.0', 'utf-8');
$data->loadXML($rawdata);

/*
 Check ob Signature in Ordnung
*/ 

//echo "Check Signature ---".PHP_EOL ;
$pgp="sudo /usr/bin/gpg "; // path to GnuPG
$tim_file = tempnam(sys_get_temp_dir(), 'gpgAdmin_tim');
$sig_file = tempnam(sys_get_temp_dir(), 'gpgAdmin_sig');
$sec_file = tempnam(sys_get_temp_dir(), 'gpgAdmin_sec');
$res_file = tempnam(sys_get_temp_dir(), 'gpgAdmin_res');
$act_file = tempnam(sys_get_temp_dir(), 'gpgAdmin_act');
$sender_file = tempnam(sys_get_temp_dir(), 'gpgAdmin_sender');
 
//echo $data->getElementsByTagName('message')->item(0)->nodeValue; 
$signature=$data->getElementsByTagName('signature')->item(0)->nodeValue;
file_put_contents($tim_file, $data->getElementsByTagName('time_stamp')->item(0)->nodeValue);
file_put_contents($sig_file, $signature);
file_put_contents($sec_file, $data->getElementsByTagName('secret')->item(0)->nodeValue);
file_put_contents($act_file, $data->getElementsByTagName('action')->item(0)->nodeValue);
 
//echo " Verify".PHP_EOL;
$command = $pgp.' --verify '.$sig_file.' '.$sec_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($result);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

//echo " Suche Sender".PHP_EOL;
$command = $pgp.' --list-packets  '.$sig_file.' | grep -i ":signature packet: algo 1, keyid " | awk \'{print $6}\' >  '.$sender_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($result);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$sender="";
$file = fopen($sender_file,"r");
while(! feof($file))
  {
  	$sender.=fgets($file);
  }
fclose($file);
$sender = preg_replace('~[\r\n]+~', '', $sender);
//echo "Sender ---".PHP_EOL;

$command = $pgp.' --decrypt --output '.$sec_file.'.out '.$sec_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
$secret=file_get_contents($sec_file.'.out');

if ($errorcode != '0') {
    var_dump($command);
    var_dump($result);
    var_dump($secret);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$command = $pgp.' --decrypt '.$tim_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
$zeitstempel="";
foreach ($output as $key => $value) {
    $zeitstempel.=$value."\n";
}
if ($errorcode != '0') {
    var_dump($command);
    var_dump($result);
    var_dump($zeitstempel);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$command = $pgp.' --decrypt '.$act_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
$action="";
foreach ($output as $key => $value) {
    $action.=$value."\n";
}
if ($errorcode != '0') {
    var_dump($command);
    var_dump($result);
    var_dump($action);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

// Protokollieren in der DB
$dbconn = pg_connect("host=/var/run/postgresql user=postgres dbname=postgres")
   or die("Keine Verbindung zur Datenbank möglich");

$sql = "SELECT nextval('pgadmin_message_id_seq')";
$result = pg_query($dbconn,  $sql);
if (!$result) {
  echo "Ein Fehler ist aufgetreten bei pgadmin_message_id_seq.\n";
  echo $sql;
  echo pg_last_error($dbconn);
  exit;
}

while ($row = pg_fetch_row($result)) {
  $pgadmin_message_id_seq = $row[0];
}

$pgadmin_status='init';
$sql = "INSERT INTO pgadmin_message
 (id, zeitstempel, signature, to_do, message, status, sender)
 VALUES ($1, $2, $3, $4, $5, $6, $7)";

$params = array ($pgadmin_message_id_seq, $zeitstempel, $signature, $action, $secret, $pgadmin_status, $sender);
$result = pg_query_params($dbconn, $sql, $params);
if (!$result) {
  echo "Ein Fehler ist aufgetreten bei pgadmin_message.\n";
  echo $sql;
  echo pg_last_error($dbconn);
  exit;
}
pg_close($dbconn);

// jetzt exekutieren wir
$pgadmin_status='success';
$log="";

if (strcmp($action, 'psql') !== 0) {
/*
$ psql --help
psql is the PostgreSQL interactive terminal.

Usage:
  psql [OPTION]... [DBNAME [USERNAME]]

General options:
  -c, --command=COMMAND    run only single command (SQL or internal) and exit
  -d, --dbname=DBNAME      database name to connect to (default: "postgres")
  -f, --file=FILENAME      execute commands from file, then exit
  -l, --list               list available databases, then exit
  -v, --set=, --variable=NAME=VALUE
                           set psql variable NAME to VALUE
                           (e.g., -v ON_ERROR_STOP=1)
  -V, --version            output version information, then exit
  -X, --no-psqlrc          do not read startup file (~/.psqlrc)
  -1 ("one"), --single-transaction
                           execute as a single transaction (if non-interactive)
  -?, --help[=options]     show this help, then exit
      --help=commands      list backslash commands, then exit
      --help=variables     list special variables, then exit

Input and output options:
  -a, --echo-all           echo all input from script
  -b, --echo-errors        echo failed commands
  -e, --echo-queries       echo commands sent to server
  -E, --echo-hidden        display queries that internal commands generate
  -L, --log-file=FILENAME  send session log to file
  -n, --no-readline        disable enhanced command line editing (readline)
  -o, --output=FILENAME    send query results to file (or |pipe)
  -q, --quiet              run quietly (no messages, only query output)
  -s, --single-step        single-step mode (confirm each query)
  -S, --single-line        single-line mode (end of line terminates SQL command)

Output format options:
  -A, --no-align           unaligned table output mode
      --csv                CSV (Comma-Separated Values) table output mode
  -F, --field-separator=STRING
                           field separator for unaligned output (default: "|")
  -H, --html               HTML table output mode
  -P, --pset=VAR[=ARG]     set printing option VAR to ARG (see \pset command)
  -R, --record-separator=STRING
                           record separator for unaligned output (default: newline)
  -t, --tuples-only        print rows only
  -T, --table-attr=TEXT    set HTML table tag attributes (e.g., width, border)
  -x, --expanded           turn on expanded table output
  -z, --field-separator-zero
                           set field separator for unaligned output to zero byte
  -0, --record-separator-zero
                           set record separator for unaligned output to zero byte

Connection options:
  -h, --host=HOSTNAME      database server host or socket directory (default: "local socket")
  -p, --port=PORT          database server port (default: "5432")
  -U, --username=USERNAME  database user name (default: "postgres")
  -w, --no-password        never prompt for password
  -W, --password           force password prompt (should happen automatically)

For more information, type "\?" (for internal commands) or "\help" (for SQL
commands) from within psql, or consult the psql section in the PostgreSQL
documentation.

Report bugs to <pgsql-bugs@lists.postgresql.org>.
PostgreSQL home page: <https://www.postgresql.org/>
*/
//	$command = 'psql -w -U postgres -d postgres -a -f '.$sec_file.'.out -e '.$sec_file.'.res -L '.$sec_file.'.log -o '.$sec_file.'.output ';
	$command = 'psql --no-password --username=postgres --dbname=postgres --echo-all  --file='.$sec_file.'.out --log-file='.$res_file;
	$output="";
	$result = exec($command, $output, $errorcode);
	if ($errorcode != '0') {
	    var_dump($command);
	    var_dump($output);
	    var_dump($errorcode);
	    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
	    $pgadmin_status='failure';
	}
	//var_dump($output);
	foreach ($output as $key => $value) {
	    $log.=$value."\n";
	}
	//var_dump($log);
}

file_put_contents($tim_file, date('c'));
//file_put_contents($sec_file, $log);
file_put_contents($act_file, $pgadmin_status);

// Protokollieren in der DB
$dbconn = pg_connect("host=/var/run/postgresql user=postgres dbname=postgres")
   or die("Keine Verbindung zur Datenbank möglich");
$sql = "UPDATE pgadmin_message SET status=$1, log=$2 WHERE id=$3";

$params = array ($pgadmin_status, $log, $pgadmin_message_id_seq);
$result = pg_query_params($dbconn, $sql, $params);

if (!$result) {
  echo "Ein Fehler ist aufgetreten bei pgadmin_message.\n";
  echo $sql;
  echo pg_last_error($dbconn);
  exit;
}
pg_close($dbconn);

// Erzeugen der Antwort
$command = $pgp.' --encrypt -a --recipient '.$sender.' --output '.$tim_file.'.asc '.$tim_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$command = $pgp.' --encrypt -a --recipient '.$sender.' --output '.$res_file.'.asc '.$res_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$command = $pgp.' --encrypt -a --recipient '.$sender.' --output '.$act_file.'.asc '.$act_file;
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$command = $pgp.' --detach-sign -a --output '.$sig_file.'.sig '.$res_file.'.asc';
//echo $command.PHP_EOL;
$output="";
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($output);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - Decryption nicht möglich");
    exit();
}

$command = 'sudo /usr/share/webapps/RestAPI/mygpg.sh "/tmp/gpgAdmin_*" ';
//echo $command.PHP_EOL;
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - CLeanup nicht möglich");
    exit();
}

$tim=file_get_contents($tim_file.'.asc');
$act=file_get_contents($act_file.'.asc');
$sig=file_get_contents($sig_file.'.sig');
$sec=file_get_contents($res_file.'.asc');

$command = ' rm '.$sig_file.'* '.$sec_file.'* '.$res_file.'* '.$act_file.'* '.$tim_file.'* '.$sender_file.'* ';
//$command = ' ls -lisa '.$sig_file.'* '.$sec_file.'* '.$act_file.'* '.$tim_file.'* '.$sender_file.'* ';
//echo $command.PHP_EOL;
$result = exec($command, $output, $errorcode);
if ($errorcode != '0') {
    var_dump($command);
    var_dump($errorcode);
    header("HTTP/1.1 404 Not Found - CLeanup nicht möglich");
    exit();
}

echo '</log>';
echo '<time_stamp>'.$tim.'</time_stamp>';
echo '<action>'.$act.'</action>';
echo '<signature>'.$sig.'</signature>';
echo '<secret>'.$sec.'</secret>';
echo '</message>';

?>
