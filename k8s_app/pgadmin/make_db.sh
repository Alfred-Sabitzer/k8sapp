#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-12-01 17:55:49 +0100 (Mi, 01. Dez 2021) $"
echo "#    $Revision: 1452 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/make_db.sh $"
echo "#    $Id: make_db.sh 1452 2021-12-01 16:55:49Z alfred $"
echo "#"
echo "# Starten und initialisieren der Postgres DB"
echo "# Das muß mit dem User postgres erfolgen"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
mkdir -p -v ${PGDATA}
mkdir -p -v /run/postgresql
which pg_ctl
echo "Setzen der richtigen Berechtigungen"
mkdir -p -v ${PGDATA}
chown -R postgres:postgres /postgres
mkdir -p -v /run/postgresql
chown -R postgres:postgres /run/postgresql
su postgres -c 'pg_ctl init'
# Nach dem Init stoppen, und dann die Config ändern
su postgres -c 'pg_ctl -D '${PGDATA}' stop'
# Kopieren der Keys
cp ${POSTGRES_TLS_CERT} ${PGDATA}/tls.crt
cp ${POSTGRES_TLS_KEY} ${PGDATA}/tls.key
# Nun ersetzen der ssl-Einträge
xdate=$(date '+%Y%m%d%H%M%S') 
echo ${xdate}
cp ${PGDATA}/postgresql.conf ${PGDATA}/postgresql.conf.${xdate}
sed -i 's,#ssl_cert_file = .*,ssl_cert_file = '\''tls.crt'\'',g' ${PGDATA}/postgresql.conf
sed -i 's,#ssl_key_file = .*,ssl_key_file = '\''tls.key'\'',g' ${PGDATA}/postgresql.conf
sed -i 's,#ssl_min_protocol_version = .*,ssl_min_protocol_version = '\''TLSv1.3'\'',g' ${PGDATA}/postgresql.conf
sed -i 's,#ssl_ciphers = ,ssl_ciphers = ,g' ${PGDATA}/postgresql.conf
sed -i 's,#ssl = .*,ssl = on,g' ${PGDATA}/postgresql.conf
sed -i 's,#ssl_prefer_server_ciphers = .*,ssl_prefer_server_ciphers = on,g' ${PGDATA}/postgresql.conf
sed -i 's,#listen_addresses = .*,listen_addresses = '\''*'\'' # Von überall her,g' ${PGDATA}/postgresql.conf
sed -i 's,#authentication_timeout = 1min,authentication_timeout = 1min,g' ${PGDATA}/postgresql.conf
sed -i 's,#password_encryption = md5,password_encryption = scram-sha-256,g' ${PGDATA}/postgresql.conf

# Richtige Berechtigungen für im Netzwerk
cp ${PGDATA}/pg_hba.conf ${PGDATA}/pg_hba.conf.${xdate}
sed -i '$a# user von local ' ${PGDATA}/pg_hba.conf
sed -i '$a# IPv4 von local: ' ${PGDATA}/pg_hba.conf
sed -i '$ahost    all             all              0.0.0.0/0           scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$ahost    all             all              ::/0           scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a###host    all             all             10.0.0.0/24           scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a###host    all             all             172.16.0.0/12         scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a###host    all             all             192.168.0.0/16        scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a#### IPv6 von local: https://en.wikipedia.org/wiki/Unique_local_address ' ${PGDATA}/pg_hba.conf
sed -i '$a####host    all             all             fd00::/8              scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a#### IPv4 von irgendwo: ' ${PGDATA}/pg_hba.conf
sed -i '$a####host    all             all             0.0.0.0/32            scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a#### IPv6 von irgendwo: ' ${PGDATA}/pg_hba.conf
sed -i '$a####host    all             all             ::/128                scram-sha-256 ' ${PGDATA}/pg_hba.conf
sed -i '$a#### Ende der speziellen Config ' ${PGDATA}/pg_hba.conf
# Richtige Berechtigungen für die Files
chown postgres:postgres ${PGDATA}/*.*
chmod 0600 ${PGDATA}/tls.*
#
su postgres -c 'pg_ctl -D '${PGDATA}' start'
echo "-"
echo "- Database gestartet "
echo "-"
cat <<EOF > /tmp/pg_reload_conf.sql
SELECT pg_reload_conf();
SHOW ssl;
\d pg_stat_ssl
\x
SELECT * FROM pg_stat_ssl;
EOF
su postgres -c 'psql -f /tmp/pg_reload_conf.sql'
rm -f /tmp/pg_reload_conf.sql
echo "-"
echo "- SSL-Settings dumped "
echo "-"
#
su postgres -c 'psql -e -f /usr/share/webapps/RestAPI/init.sql'
#
cat <<EOF > /tmp/create_user.sql
-- Anlegen des Users
select create_superuser('${POSTGRES_USER}','${POSTGRES_PASSWORD}');
select set_password('${POSTGRES_USER}','${POSTGRES_PASSWORD}'); -- Bei Neustart wird das Passwort wieder initialisert
-- Anlegen der Datenbank
select create_database('${POSTGRES_USER}','${POSTGRES_USER}');
select grant_database_privileges('ALL','${POSTGRES_USER}','${POSTGRES_USER}');
select * from pg_catalog.pg_database;
select * from pg_catalog.pg_user;
EOF
su postgres -c 'psql -e -f /tmp/create_user.sql'
rm -f /tmp/create_user.sql
cat <<EOF > /tmp/alter_db.sql
-- Kommentar
COMMENT ON DATABASE ${POSTGRES_USER} IS 'Standard-Datenbank für ${POSTGRES_USER}';
EOF
su postgres -c 'psql -e -f /tmp/alter_db.sql'
rm -f /tmp/alter_db.sql
#

