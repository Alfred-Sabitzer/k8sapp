---
# Yaml für ${image}:${tag}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${image}-depl
  namespace: ${namespace}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${image}-app
  minReadySeconds: 60
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: ${image}-app
      annotations:
        sidecar.istio.io/inject: "false"
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - ${image}-app
            topologyKey: "kubernetes.io/hostname"
      volumes:
        - name: ${image}-postgres
          persistentVolumeClaim:
            claimName: ${image}-pvc
        - name: config-volume
          configMap:
            name: pgadmin-config-cm
        - name: pgp-volume
          configMap:
            name: pgadmin-pgp-cm
        - name: certificate-volume
          secret:
            secretName: ${secretName}
        - name:  pgadmin
          secret:
            secretName: pgadmin
            
      terminationGracePeriodSeconds: 20
      containers:
      - name: ${image}
        image: ${docker_registry}/${image}:${tag} # Datenbank ist stateful, daher immer redeploy wenn sich woanders was ändert
        imagePullPolicy: Always
        env:
        - name: image
          value: "${image}"
        - name: tag
          value: "${tag}"
        - name: TZ
          value: Europe/Vienna
        - name: LANG
          value: de_DE.UTF-8
        - name: LANGUAGE
          value: de_DE.UTF-8
        - name: LC_ALL
          value: de_DE.UTF-8
        - name: PGDATA
          value: /postgres/data
        - name: POSTGRES_TLS_CERT
          value: /certificate/tls.crt
        - name: POSTGRES_TLS_KEY
          value: /certificate/tls.key
        - name: LIGHTTPD_INC
          value: /config/lighttpd.conf
        - name: PHPADMIN_INC
          value: /config/config.inc.php

        ports:
        - containerPort: 5432
          name: postgres
        - containerPort: 80
          name: lighttpd
        - containerPort: 443
          name: lighttpds
        volumeMounts:
        - mountPath: /postgres
          name: ${image}-postgres
        - name: config-volume
          mountPath: /config
          readOnly: true
        - name: pgp-volume
          mountPath: /pgp
          readOnly: true
        - name: certificate-volume
          mountPath: /certificate
          readOnly: true
        - name:  pgadmin
          mountPath: /pgadmin
          readOnly: true
        # check for lifetime liveness, restarts if dead
        livenessProbe:
          exec:
            command: ["psql", "-w", "-U", "postgres", "-d", "postgres", "-c", "SELECT 1"]
          initialDelaySeconds: 120
          timeoutSeconds: 5
          periodSeconds: 30
          successThreshold: 1
          failureThreshold: 3
        # check for initial readiness
        readinessProbe:
          exec:
            command:
            - /bin/sh
            - -c
            - exec pg_isready -U "postgres" -h 127.0.0.1 -p 5432
          initialDelaySeconds: 120
          timeoutSeconds: 5
          periodSeconds: 30
          successThreshold: 1
          failureThreshold: 3       
      # Verhalten, beim Beenden
      terminationGracePeriodSeconds: 60
      restartPolicy: Always
      dnsPolicy: ClusterFirst
---
