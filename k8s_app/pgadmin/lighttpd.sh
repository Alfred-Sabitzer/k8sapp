#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/lighttpd.sh $"
echo "#    $Id: lighttpd.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Konfiguration und starten des Lighthttpd "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
echo ${HOSTNAME}
cat ${POSTGRES_TLS_KEY}  ${POSTGRES_TLS_CERT} > /etc/lighttpd/${HOSTNAME}.pem
sed 's,##HOSTNAME##,'${HOSTNAME}',g' ${LIGHTTPD_INC} > /etc/lighttpd/lighttpd.conf
#sed -i 's,.*include "mod_fastcgi.conf",   include "mod_fastcgi.conf",g' /etc/lighttpd/lighttpd.conf
#sed -i 's,var.basedir.*,var.basedir  = "/usr/share/webapps/phpPgAdmin-7.13.0/",g' /etc/lighttpd/lighttpd.conf
#sed -i 's,server.document-root.*,server.document-root = var.basedir,g' /etc/lighttpd/lighttpd.conf
#sed -i 's,var.logdir.*,var.logdir = "/dev/stdout",g' /etc/lighttpd/lighttpd.conf
#sed -i 's,server.errorlog.*,server.errorlog = "/dev/stdout"  #default to stdout,g' /etc/lighttpd/lighttpd.conf
#sed -i 's,accesslog.filename.*,accesslog.filename = "/dev/stdout" # default to stdout,g' /etc/lighttpd/lighttpd.conf
#sed -i 's,#    "mod_status",    "mod_status",g' /etc/lighttpd/lighttpd.conf
#sed -i 's,# status.status-url, status.status-url,g' /etc/lighttpd/lighttpd.conf
#sed -i 's,# status.config-url, status.config-url,g' /etc/lighttpd/lighttpd.conf
#sed -i '$ a #' /etc/lighttpd/lighttpd.conf
#sed -i '$ a # {{{ mod_status' /etc/lighttpd/lighttpd.conf
#sed -i '$ a # see status.txt' /etc/lighttpd/lighttpd.conf
#sed -i '$ a # Ermöglichen der Status-URLs im Cluster und im lokalen Netzwerk' /etc/lighttpd/lighttpd.conf
#sed -i '$ a #' /etc/lighttpd/lighttpd.conf
#sed -i '$ a $HTTP["remoteip"] == "10.0.0.0/8" { status.status-url = "/server-status" }' /etc/lighttpd/lighttpd.conf
#sed -i '$ a $HTTP["remoteip"] == "10.0.0.0/8" { status.config-url = "/server-config" }'     /etc/lighttpd/lighttpd.conf
#sed -i '$ a $HTTP["remoteip"] == "10.0.0.0/8" { status.statistics-url = "/server-statistics" }' /etc/lighttpd/lighttpd.conf
#sed -i '$ a $HTTP["remoteip"] == "192.168.0.0/8" { status.status-url = "/server-status" }' /etc/lighttpd/lighttpd.conf
#sed -i '$ a $HTTP["remoteip"] == "192.168.0.0/8" { status.config-url = "/server-config" }' /etc/lighttpd/lighttpd.conf
#sed -i '$ a $HTTP["remoteip"] == "192.168.0.0/8" { status.statistics-url = "/server-statistics" }' /etc/lighttpd/lighttpd.conf
#sed -i '$ a # }}}' /etc/lighttpd/lighttpd.conf
# Start lighttpd mit Ausgabe auf stdout
chmod o+w /dev/stdout
/usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf 2>&1
#

