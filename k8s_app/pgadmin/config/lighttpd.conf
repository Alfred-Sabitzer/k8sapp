# {{{ variables
var.basedir  = "/usr/share/webapps/phpPgAdmin-7.13.0/"
#var.logdir = "/dev/stdout"
var.logdir = "/var/log/lighttpd/"
var.statedir = "/var/lib/lighttpd"
# }}}

alias.url = ( "/RestAPI/" => "/usr/share/webapps/RestAPI/" )

# {{{ modules
# At the very least, mod_access and mod_accesslog should be enabled.
# All other modules should only be loaded if necessary.
# NOTE: the order of modules is important.
server.modules = (
#    "mod_rewrite",
    "mod_redirect",
    "mod_alias",
    "mod_access",
    "mod_openssl",
#    "mod_cml",
#    "mod_trigger_b4_dl",
#    "mod_auth",
    "mod_status",
#    "mod_setenv",
#    "mod_proxy",
#    "mod_simple_vhost",
#    "mod_evhost",
#    "mod_userdir",
#    "mod_compress",
#    "mod_ssi",
#    "mod_usertrack",
#    "mod_expire",
#    "mod_secdownload",
#    "mod_rrdtool",
#    "mod_webdav",
    "mod_accesslog"
)
# }}}

# {{{ includes
include "mime-types.conf"
# uncomment for cgi support
#   include "mod_cgi.conf"
# uncomment for php/fastcgi support
   include "mod_fastcgi.conf"
# uncomment for php/fastcgi fpm support
#   include "mod_fastcgi_fpm.conf"
# }}}

# {{{ server settings
server.username      = "lighttpd"
server.groupname     = "lighttpd"

server.document-root = var.basedir
server.pid-file      = "/run/lighttpd.pid"

# server.errorlog = "/dev/stdout"  #default to stdout
# server.errorlog = var.logdir+"error.log"
# log errors to syslog instead
# server.errorlog = "/dev/stdout"  #default to stdout

server.indexfiles    = ("index.php", "index.html",
						"index.htm", "default.htm")

# server.tag           = "lighttpd"

server.follow-symlink = "enable"

# event handler (defaults to "poll")
# see performance.txt
# 
# for >= linux-2.4
#   server.event-handler = "linux-rtsig"
# for >= linux-2.6
#   server.event-handler = "linux-sysepoll"
# for FreeBSD
#   server.event-handler = "freebsd-kqueue"

# chroot to directory (defaults to no chroot)
# server.chroot      = "/"

# bind to port (defaults to 80)
# server.port          = 81

# bind to name (defaults to all interfaces)
# server.bind          = "grisu.home.kneschke.de"

# error-handler for status 404
# server.error-handler-404 = "/error-handler.html"
# server.error-handler-404 = "/error-handler.php"

# Format: <errorfile-prefix><status-code>.html
# -> ..../status-404.html for 'File not found'
# server.errorfile-prefix    = var.basedir  = "/usr/share/webapps/phpPgAdmin-7.13.0/"

# FAM support for caching stat() calls
# requires that lighttpd be built with USE=fam
#   server.stat-cache-engine = "fam"
# }}}

# {{{ mod_staticfile

# which extensions should not be handled via static-file transfer
# (extensions that are usually handled by mod_cgi, mod_fastcgi, etc).
static-file.exclude-extensions = (".php", ".pl", ".cgi", ".fcgi")
# }}}

# {{{ mod_accesslog
# accesslog.filename = var.logdir+"access.log"
 accesslog.filename = "/dev/stderr"

# }}}

# {{{ mod_dirlisting
# enable directory listings
#   dir-listing.activate      = "enable"
#
# don't list hidden files/directories
#   dir-listing.hide-dotfiles = "enable"
#
# use a different css for directory listings
#   dir-listing.external-css  = "/path/to/dir-listing.css"
#
# list of regular expressions.  files that match any of the
# specified regular expressions will be excluded from directory
# listings.
#   dir-listing.exclude = ("^\.", "~$")
# }}}

# {{{ mod_access
# see access.txt

url.access-deny = ("~", ".inc")
# }}}

# {{{ mod_userdir
# see userdir.txt
#
# userdir.path = "public_html"
# userdir.exclude-user = ("root")
# }}}

# {{{ mod_ssi
# see ssi.txt
#
# ssi.extension = (".shtml")
# }}}

# {{{ mod_ssl
# see ssl.txt
#
# ssl.engine    = "enable"
# ssl.pemfile   = "server.pem"
# }}}

# {{{ mod_status
# see status.txt
#
 status.status-url  = "/server-status"
 status.config-url  = "/server-config"
# }}}

# {{{ mod_simple_vhost
# see simple-vhost.txt
#
#  If you want name-based virtual hosting add the next three settings and load
#  mod_simple_vhost
#
# document-root =
#   virtual-server-root + virtual-server-default-host + virtual-server-docroot
# or
#   virtual-server-root + http-host + virtual-server-docroot
#
# simple-vhost.server-root   = "/home/weigon/wwwroot/servers/"
# simple-vhost.default-host  = "grisu.home.kneschke.de"
# simple-vhost.document-root = "/pages/"
# }}}

# {{{ mod_compress
# see compress.txt
#
# compress.cache-dir   = var.statedir + "/cache/compress"
# compress.filetype    = ("text/plain", "text/html")
# }}}

# {{{ mod_proxy
# see proxy.txt
#
# proxy.server               = ( ".php" =>
#                               ( "localhost" =>
#                                 (
#                                   "host" => "192.168.0.101",
#                                   "port" => 80
#                                 )
#                               )
#                             )
# }}}

# {{{ mod_auth
# see authentication.txt
#
# auth.backend               = "plain"
# auth.backend.plain.userfile = "lighttpd.user"
# auth.backend.plain.groupfile = "lighttpd.group"

# auth.backend.ldap.hostname = "localhost"
# auth.backend.ldap.base-dn  = "dc=my-domain,dc=com"
# auth.backend.ldap.filter   = "(uid=$)"

# auth.require               = ( "/server-status" =>
#                               (
#                                 "method"  => "digest",
#                                 "realm"   => "download archiv",
#                                 "require" => "user=jan"
#                               ),
#                               "/server-info" =>
#                               (
#                                 "method"  => "digest",
#                                 "realm"   => "download archiv",
#                                 "require" => "valid-user"
#                               )
#                             )
# }}}

# {{{ mod_rewrite
# see rewrite.txt
#
# url.rewrite = (
#	"^/$"		=>		"/server-status"
# )
# }}}

# {{{ mod_redirect
# see redirect.txt
#
# url.redirect = (
#	"^/wishlist/(.+)"		=>		"http://www.123.org/$1"
# )
# }}}

# {{{ mod_evhost
# define a pattern for the host url finding
# %% => % sign
# %0 => domain name + tld
# %1 => tld
# %2 => domain name without tld
# %3 => subdomain 1 name
# %4 => subdomain 2 name
#
# evhost.path-pattern        = "/home/storage/dev/www/%3/htdocs/"
# }}}

# {{{ mod_expire
# expire.url = (
#	"/buggy/"		=>		"access 2 hours",
#	"/asdhas/"		=>		"access plus 1 seconds 2 minutes"
# )
# }}}

# {{{ mod_rrdtool
# see rrdtool.txt
#
# rrdtool.binary  = "/usr/bin/rrdtool"
# rrdtool.db-name = var.statedir + "/lighttpd.rrd"
# }}}

# {{{ mod_setenv
# see setenv.txt
#
# setenv.add-request-header  = ( "TRAV_ENV" => "mysql://user@host/db" )
# setenv.add-response-header = ( "X-Secret-Message" => "42" )
# }}}

# {{{ mod_trigger_b4_dl
# see trigger_b4_dl.txt
#
# trigger-before-download.gdbm-filename = "/home/weigon/testbase/trigger.db"
# trigger-before-download.memcache-hosts = ( "127.0.0.1:11211" )
# trigger-before-download.trigger-url = "^/trigger/"
# trigger-before-download.download-url = "^/download/"
# trigger-before-download.deny-url = "http://127.0.0.1/index.html"
# trigger-before-download.trigger-timeout = 10
# }}}

# {{{ mod_cml
# see cml.txt
#
# don't forget to add index.cml to server.indexfiles
# cml.extension               = ".cml"
# cml.memcache-hosts          = ( "127.0.0.1:11211" )
# }}} 

# {{{ mod_webdav
# see webdav.txt
#
# $HTTP["url"] =~ "^/dav($|/)" {
#     webdav.activate = "enable"
#     webdav.is-readonly = "enable"
# }
# }}}

# {{{ extra rules
#
# set Content-Encoding and reset Content-Type for browsers that
# support decompressing on-thy-fly (requires mod_setenv)
# $HTTP["url"] =~ "\.gz$" {
#     setenv.add-response-header = ("Content-Encoding" => "x-gzip")
#     mimetype.assign = (".gz" => "text/plain")
# }

# $HTTP["url"] =~ "\.bz2$" {
#     setenv.add-response-header = ("Content-Encoding" => "x-bzip2")
#     mimetype.assign = (".bz2" => "text/plain")
# }
#
# }}}

# {{{ debug
# debug.log-request-header   = "enable"
# debug.log-response-header  = "enable"
# debug.log-request-handling = "enable"
# debug.log-file-not-found   = "enable"
# }}}

$SERVER["socket"] == ":80" {
  $HTTP["host"] =~ ".*" {
    url.redirect = (".*" => "https://%0$0")
  }
}

$SERVER["socket"] == ":443" {
        ssl.engine = "enable"
        ssl.pemfile = "/etc/lighttpd/##HOSTNAME##.pem"
      # ssl.ca-file = "/etc/lighttpd/ssl/CA_issuing.crt"
        server.name = "##HOSTNAME##"
        server.document-root = "/usr/share/webapps/phpPgAdmin-7.13.0/"
      #  accesslog.filename =var.logdir+"access.log"
        accesslog.filename = "/dev/stderr"
      #  server.errorlog =var.logdir+"error.log"
      #  server.errorlog = "/dev/stderr"
}        

# vim: set ft=conf foldmethod=marker et :
#
# {{{ mod_status
# see status.txt
# Ermöglichen der Status-URLs im Cluster und im lokalen Netzwerk
#
$HTTP["remoteip"] == "127.0.0.1" { status.status-url = "/server-status" }
$HTTP["remoteip"] == "127.0.0.1" { status.config-url = "/server-config" }
$HTTP["remoteip"] == "127.0.0.1" { status.statistics-url = "/server-statistics" }
$HTTP["remoteip"] == "172.0.0.1" { status.status-url = "/server-status" }
$HTTP["remoteip"] == "172.0.0.1" { status.config-url = "/server-config" }
$HTTP["remoteip"] == "172.0.0.1" { status.statistics-url = "/server-statistics" }
$HTTP["remoteip"] == "10.0.0.0/8" { status.status-url = "/server-status" }
$HTTP["remoteip"] == "10.0.0.0/8" { status.config-url = "/server-config" }
$HTTP["remoteip"] == "10.0.0.0/8" { status.statistics-url = "/server-statistics" }
$HTTP["remoteip"] == "192.168.0.0/8" { status.status-url = "/server-status" }
$HTTP["remoteip"] == "192.168.0.0/8" { status.config-url = "/server-config" }
$HTTP["remoteip"] == "192.168.0.0/8" { status.statistics-url = "/server-statistics" }
# }}}
