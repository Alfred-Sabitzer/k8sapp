#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/set_env.sh $"
echo "#    $Id: set_env.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Setzen der Environment-Variablen aus dem K8s-Secret "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
export POSTGRES_PASSWORD=$(cat /pgadmin/password)
export POSTGRES_USER=$(cat /pgadmin/username)
export POSTGRES_DB=$(cat /pgadmin/username)
#
