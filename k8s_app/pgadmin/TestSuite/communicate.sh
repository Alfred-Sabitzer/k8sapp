#!/bin/bash
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/TestSuite/communicate.sh $"
echo "#    $Id: communicate.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Kommunikation mit dem RestAPI "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

#hostname='monitoring:8443'
hostname='localhost:8443'
localuser='alfred@slainte.at'
#localuser='alfred.sabitzer@ainet.at'
recipient='k8s.postgres.allgemein@slainte.at'

secret="/tmp/secret.txt"
action="/tmp/action.txt"
xdate="/tmp/date.txt"
message="/tmp/message.xml"

rm -f ${xdate}*
rm -f ${action}*
rm -f ${secret}*
rm -f ${message}*

cat <<EOF > ${action}
psql
EOF

echo $(date +"%Y-%m-%d %H:%M:%S %s") > ${xdate}

cat <<EOF > ${secret}
-- Anlegen des Users
select create_user('franzi','franzi');
select set_password('franzi','franzi');
-- Anlegen der Datenbank
select create_database('franzi','franzi');
select grant_database_privileges('ALL','franzi','franzi');
select * from pg_catalog.pg_database;
select * from pg_catalog.pg_user;
EOF

gpg --encrypt -a --local-user ${localuser} --recipient ${recipient} --output ${xdate}.asc ${xdate}
gpg --encrypt -a --local-user ${localuser} --recipient ${recipient} --output ${action}.asc ${action} 
gpg --encrypt -a --local-user ${localuser} --recipient ${recipient} --output ${secret}.asc  ${secret} 
gpg --detach-sign -a --local-user ${localuser} --output ${secret}.sig ${secret}.asc
gpg --verify ${secret}.sig ${secret}.asc

#
# Erzeugen der Nachricht
#
cat <<EOF > ${message}
<?xml version="1.0" standalone="yes"?>
<message>
<time_stamp>$(cat ${xdate}.asc)
</time_stamp>
<action>$(cat ${action}.asc)
</action>
<signature>$(cat ${secret}.sig)
</signature>
<secret>$(cat ${secret}.asc)
</secret>
</message>
EOF
#
# Übertragen der Nachricht
#
curl -k --insecure --data-binary @${message} -H "Content-Type: application/xml" https://${hostname}/RestAPI/ > /tmp/result.xml
exit
