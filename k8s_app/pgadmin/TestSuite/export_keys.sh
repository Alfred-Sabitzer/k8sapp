#!/bin/bash
############################################################################################
#    $Date: 2021-12-01 17:07:55 +0100 (Mi, 01. Dez 2021) $
#    $Revision: 1450 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/TestSuite/export_keys.sh $
#    $Id: export_keys.sh 1450 2021-12-01 16:07:55Z alfred $
#
# exportieren der pgp-keys und des ownertrusts
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
#shopt -o -s nounset #-No Variables without definition
gpg --list-keys
gpg --batch --yes --armor --output alfred_slainte_at.asc --export alfred@slainte.at
gpg --batch --yes --armor --output k8s_postgres_allgemein_slainte_at.asc --export k8s.postgres.allgemein@slainte.at
gpg --batch --yes --armor --output k8s_postgres_allgemein_slainte_at_private.asc --export-secret-key k8s.postgres.allgemein@slainte.at
gpg --batch --yes --armor --export-ownertrust > trust.txt 
#gpg --yes --armor --output k8s_postgres_allgemein_slainte_at.rev --generate-revocation k8s.postgres.allgemein@slainte.at
#
