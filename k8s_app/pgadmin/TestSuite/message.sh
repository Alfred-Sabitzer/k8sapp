#!/bin/bash
############################################################################################
#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $
#    $Revision: 1427 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/TestSuite/message.sh $
#    $Id: message.sh 1427 2021-11-28 19:15:12Z alfred $
#
# Nachricht senden und empfangen
#
############################################################################################
#curl -k --insecure --data-binary @message.xml -H "Content-Type: application/xml" https://localhost/RestAPI/
curl -k --insecure --data-binary @message.xml -H "Content-Type: application/xml" https://monitoring/RestAPI/
