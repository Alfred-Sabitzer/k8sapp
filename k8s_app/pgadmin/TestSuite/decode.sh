#!/bin/bash
echo "############################################################################################"
echo "#    $Date: 2021-11-28 20:15:12 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1427 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/TestSuite/decode.sh $"
echo "#    $Id: decode.sh 1427 2021-11-28 19:15:12Z alfred $"
echo "#"
echo "# Entschlüsseln der Antwort "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

decrypt()
{
  FILE=$1
  MAGIC_WORD=$2
  OUTFILE=$3
  
  grep -Ezo '(?'${MAGIC_WORD}')[^<]+' ${FILE} > ${OUTFILE}
  sed -i 's,'${MAGIC_WORD}',,g' ${OUTFILE}
  sed -i '/-----END PGP MESSAGE-----/q' ${OUTFILE}
  rm -f ${OUTFILE}.txt
  gpg --decrypt --output ${OUTFILE}.txt ${OUTFILE}
}

grep -Ezo '(?<signature>)[^<]+' "./result.xml" > /tmp/signature.asc
sed 's,<signature>,,g' /tmp/signature.asc > /tmp/signature2.txt
sed '/-----END PGP SIGNATURE-----/q' /tmp/signature2.txt > /tmp/signature.asc

grep -Ezo '(?<secret>)[^<]+' "./result.xml" > /tmp/secret.asc
sed 's,<secret>,,g' /tmp/secret.asc > /tmp/secret2.txt
sed '/-----END PGP MESSAGE-----/q' /tmp/secret2.txt > /tmp/secret.asc

gpg --verify /tmp/signature.asc /tmp/secret.asc
if [ $? -eq 0 ]; 
then 
    echo "Gültige Signatur" 
else 
    echo "Ungültige Signatur"
    exit -1
fi

decrypt "./result.xml" "<time_stamp>" /tmp/time_stamp.asc
decrypt "./result.xml" "<action>" /tmp/action.asc
decrypt "./result.xml" "<secret>" /tmp/secret.asc

