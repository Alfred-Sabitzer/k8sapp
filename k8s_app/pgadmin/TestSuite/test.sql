-- Anlegen des Users
select * from pg_catalog.pg_user;
select create_user('franzi','franzi');
select set_passwort('franzi','franzi','franzi');
select * from pg_catalog.pg_user;

-- Anlegen der Datenbank
select create_database('franzi','franzi');
select grant_database_privileges_('ALL','franzi','franzi');
select * from pg_catalog.pg_database;

