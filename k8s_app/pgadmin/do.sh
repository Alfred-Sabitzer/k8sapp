#!/bin/bash
############################################################################################
#    $Date: 2021-11-28 21:18:08 +0100 (So, 28. Nov 2021) $
#    $Revision: 1430 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/pgadmin/do.sh $
#    $Id: do.sh 1430 2021-11-28 20:18:08Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
postgres_dir="/home/alfred/Downloads/postgres/"
svn update
source env.sh
docker build . -t ${image}:latest
#docker save ${image}:latest | gzip > ${image}_latest.tar.gz
docker kill $(docker ps | grep -i ${image}:latest | awk '{print $1 }') 
docker container rm -f $(docker container ls -a | grep -i ${image}_latest | awk '{print $1 }') 
docker run -d --name ${image}_latest -p 8443:443 -p 5432:5432 -v ${postgres_dir}:/postgres ${image}:latest
# volume /postgres
sleep 10
docker ps
docker export $(docker ps | grep -i ${image}:latest | awk '{print $1 }') | gzip >  ${image}_latest_container.tar.gz
echo "docker exec -u 0 -it ${image}_latest /bin/sh"
#docker kill $(docker ps | grep -i ${image}:latest | awk '{print $1 }') 
#docker container rm $(docker container ls -a | awk '{print $1 }') 
#
#docker run -it --name ${image}_latest -p 5432:5432 -v ${postgres_dir}:/postgres ${image}:latest /bin/bash
