// Ausgabe eines einfachen HTML-Files 
// Inspiration https://golang.org/doc/articles/wiki/

package main

import (
	"fmt"
	"strings"	
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
)

type Page struct {
	Title string
	Body  []byte
}

func loadPage(title string) (*Page, error) {
	var xhtml []byte
	var err error
	
        title = strings.Replace(title, "default/", "", -1)
        title = strings.Replace(title, "default", "", -1)
	xhtml, err = ioutil.ReadFile(title)

	if err != nil { // wenn was schiefgeht (warum auch immer) wird das index.html gezeigt
		log.Println("GET /index.html: " + title )
		xhtml, err = ioutil.ReadFile("index.html")
	}
	//log.Println("RESULT " + title )
	return &Page{Title: title, Body: []byte(xhtml)}, err
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/"):]
	//log.Println("URL " + title + "\n")
	p, _ := loadPage(title)
	fmt.Fprintf(w, "%s", p.Body)
}

func logRequest(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	        x, err := httputil.DumpRequest(r, true)
	        if err != nil {
	            http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
	            return
	        }
	        title := r.URL.Path[len("/"):]
	        title = strings.Replace(title, "default/", "", -1)
	        title = strings.Replace(title, "default", "", -1)
		if title != "healthz" {  // Nicht das Log zumüllen.
		        log.Println(fmt.Sprintf("%q", x))
		}
		handler.ServeHTTP(w, r)
	})
}

func main() {
	log.Println("Main Started")
	http.HandleFunc("/", viewHandler)
	log.Fatal(http.ListenAndServe(":8080", logRequest(http.DefaultServeMux)))
	log.Println("Main End")
}
