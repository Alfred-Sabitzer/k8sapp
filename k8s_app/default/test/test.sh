#!/bin/bash
############################################################################################
#    $Date: 2021-11-27 14:28:59 +0100 (Sa, 27. Nov 2021) $
#    $Revision: 1374 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/default/test/test.sh $
#    $Id: test.sh 1374 2021-11-27 13:28:59Z alfred $
#
# test aller möglichen Verbindungen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Slainte
#
curl -k -v http://k8s.slainte.at/default		# liefert 308 - Permanent verzogen auf https://
curl -k -v https://k8s.slainte.at/default		# liefert 200 - Zeigt die index.html an
curl -k -v https://k8s.slainte.at/default/		# liefert 200 - Zeigt die index.html an
curl -k -v https://k8s.slainte.at/default/index.html	# liefert 200 - Zeigt die index.html an
curl -k -v https://k8s.slainte.at/default/healthz	# liefert 200 - Zeigt die index.html an
curl -k -v https://k8s.slainte.at/default/unbekannt	# liefert 200 - Zeigt die index.html an
#
# default
#
curl -k -v http://default.k8s.slainte.at/default		# liefert 308 - Permanent verzogen auf https://
curl -k -v https://default.k8s.slainte.at/default		# liefert 200 - Zeigt die index.html an
curl -k -v https://default.k8s.slainte.at/default/		# liefert 200 - Zeigt die index.html an
curl -k -v https://default.k8s.slainte.at/default/index.html	# liefert 200 - Zeigt die index.html an
curl -k -v https://default.k8s.slainte.at/default/healthz	# liefert 200 - Zeigt die index.html an
curl -k -v https://default.k8s.slainte.at/default/unbekannt	# liefert 200 - Zeigt die index.html an
#

