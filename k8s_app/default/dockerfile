ARG  BUILDER_IMAGE=golang:alpine
############################
# STEP 1 build executable binary
############################
FROM ${BUILDER_IMAGE} as builder


# timezone support
ENV TZ=Europe/Vienna

# Timezone support
RUN apk add git \
	tzdata && \
	cp /usr/share/zoneinfo/${TZ} /etc/localtime &&\
	echo $TZ > /etc/timezone

# Create appuser
ENV USER=appuser
ENV UID=10001

# See https://stackoverflow.com/a/55757473/12429735
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR $GOPATH/src
# Copy the code
COPY src/ /go/src/

# Fetch dependencies.
RUN go mod init main
RUN go mod tidy
RUN go get -d -v

# Build the binary
RUN CGO_ENABLED=0 go build \
    -ldflags='-w -s -extldflags "-static"' -a \
    -o /go/bin/main .

############################
# STEP 2 build a small image
############################
FROM scratch
#FROM alpine:latest -> Zum testen ist das ganz praktisch

LABEL maintainer="microk8s.raspberry@slainte.at"
LABEL Description="Kleiner statischer Webserver"
   
# Import from builder.
#COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/localtime /etc/localtime
COPY --from=builder /etc/timezone /etc/timezone
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy html
COPY --chown=appuser:appuser html/ /html/

# Copy our static executable
COPY --from=builder /go/bin/main /main

EXPOSE 8080

# Use an unprivileged user.
USER appuser:appuser
WORKDIR /html/

# Run the hello binary.
ENTRYPOINT ["/main"]
