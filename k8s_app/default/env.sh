#!/bin/bash
############################################################################################
#    $Date: 2021-12-02 12:11:44 +0100 (Do, 02. Dez 2021) $
#    $Revision: 1479 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/default/env.sh $
#    $Id: env.sh 1479 2021-12-02 11:11:44Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="default"
export namespace="default slainte admin"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
