#!/usr/bin/bash
# Erzeugen der yaml-Dateien
#
# Das sind vorbereitenden Schritte. Müssen vorher händisch gemacht und kontrolliert werden
#
# kpt fn eval --image gcr.io/kpt-fn/search-replace:v0.2.0 -- by-value-regex='\$\{image\}-(.*)' put-comment='kpt-set: ${image}-${1}'
# kpt fn eval --image gcr.io/kpt-fn/create-setters:v0.1.0 --fn-config ./allgemein/kpt-setters.yaml
#


Environment=$1

# Überprüfen der Parameter
if [ "${Environment} " = " " ] ;
then
	echo "Usage: $0 Umgebung "
	echo "eg: $0	default"
	exit -1
fi

# Allgemeine Konfiguration
rm -fr ./output
mkdir ./output

# Alle yamls in eines zusammenkopieren
FILES="*.yaml
*.YAML"
for filename in ${FILES} ; do
  if [ -f "${filename}" ]
  then
    echo "---" >> ./output/default.yaml
    cat ${filename} >> ./output/default.yaml
    echo "---" >> ./output/default.yaml
  fi
done

cp -r ./allgemein/* ./output
cd ./output
kpt pkg tree
kpt fn render
kustomize build > default.kustomize
cat default.kustomize > default.yaml

# Nun kommt das Umgebungspezifische
cd ..
cp -rf ./${Environment}/* ./output
cd ./output
kpt pkg tree
kpt fn render
kustomize build > default.kustomize
cat default.kustomize > default.yaml

# Jetzt kommt das Deployment
kubectl apply -f default.yaml
