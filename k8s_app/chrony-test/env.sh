#!/bin/bash
############################################################################################
#    $Date: 2021-11-28 11:58:44 +0100 (So, 28. Nov 2021) $
#    $Revision: 1413 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/chrony-test/env.sh $
#    $Id: env.sh 1413 2021-11-28 10:58:44Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="chrony-test"
export namespace="slainte"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
