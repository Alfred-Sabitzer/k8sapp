#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-11-28 12:18:36 +0100 (So, 28. Nov 2021) $"
echo "#    $Revision: 1415 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/chrony-test/chrony_test.sh $"
echo "#    $Id: chrony_test.sh 1415 2021-11-28 11:18:36Z alfred $"
echo "#"
echo "# Starten des Chrony-test-Servers"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
echo "Anzeige des root-directories"
ls -lisa /
echo "Kopieren der Secrets"
#ls -lisaR 
/usr/bin/cp -v configmap/authorized_keys .ssh/
sudo chown ubuntu:root .ssh/*
sudo /usr/bin/cp -v configmap/sshd_config.conf /etc/ssh/sshd_config
sudo chown root:root /etc/ssh/*
echo "Starten des sshd-Daemons"
sudo /usr/sbin/sshd -D &
echo "Anzeige der Prozesse"
ps -ef
echo "Anzeige des Environments"
export
echo "Keep-Alive-Logging"
while [ true ]
do
	echo "`date` [`hostname`] $Id: chrony_test.sh 1415 2021-11-28 11:18:36Z alfred $"
	sudo ntpdate -v chrony-udp-svc.slainte
	sleep $((300 + RANDOM % 11));
done
echo "Das wird nie passieren"
