#!/bin/bash
############################################################################################
#    $Date: 2021-12-02 11:50:47 +0100 (Do, 02. Dez 2021) $
#    $Revision: 1476 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/namespace/slainte_env.sh $
#    $Id: slainte_env.sh 1476 2021-12-02 10:50:47Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export secretName="k8s-slainte-at-tls"
export host="k8s.slainte.at"
export namespace_comment="Namespace für die Produktion"
export cluster_issuer="letsencrypt-prod"
export docker_registry="docker.registry:5000"
#
