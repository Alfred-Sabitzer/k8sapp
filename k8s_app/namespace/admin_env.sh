#!/bin/bash
############################################################################################
#    $Date: 2021-12-19 11:11:08 +0100 (So, 19. Dez 2021) $
#    $Revision: 1658 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/namespace/admin_env.sh $
#    $Id: admin_env.sh 1658 2021-12-19 10:11:08Z alfred $
#
# Environment-Settings
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export secretName="k8s-slainte-at-tls"
export host="k8s.slainte.at"
export namespace_comment="Namespace für administrative Zwecke."
export cluster_issuer="letsencrypt-prod"
export docker_registry="docker.registry:5000"
#
