#!/bin/bash
############################################################################################
#
# Installation der Gitlab-Runner
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Alles sauber machen:)
microk8s kubectl delete -n gitlab-runner deployment $(microk8s kubectl get deployment -n gitlab-runner | grep -v NAME | awk '{print $1 }' )
microk8s kubectl delete -n gitlab-runner replicaset $(microk8s kubectl get replicaset -n gitlab-runner | grep -v NAME | awk '{print $1 }' )
microk8s kubectl delete -n gitlab-runner pod $(microk8s kubectl get pod -n gitlab-runner | grep -v NAME | awk '{print $1 }' )
microk8s kubectl delete -f gitlab-runner_namespace.yaml
sleep 10
# Erzeugen des Namespaces
microk8s kubectl apply -f gitlab-runner_namespace.yaml
# Erzeugen und registrieren aller GitRunner
declare -A Registration_Tokens
declare -A tags
loopval=( k8s-app ldap ) # Keine Underscores, a lowercase RFC 1123 subdomain must consist of lower case alphanumeric characters, '-' or '.', and must start and end with an alphanumeric character (e.g. 'example.com', regex used for validation is '[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*')
Registration_Tokens[k8s-app]="E8kHn-yMEhw5roidyTq3"
Registration_Tokens[ldap]="imefYxb2LbVCPzSEyyKy"
tags[k8s-app]="microk8s"
tags[ldap]="ldap"
cp values.yaml /tmp/values.yaml
for i in "${loopval[@]}"
do
	echo "${i}"
	echo "${Registration_Tokens[${i}]}"
	echo "${tags[${i}]}"
	RunnerName="gr-${i}"
	sed -i "s,runnerRegistrationToken: .*,runnerRegistrationToken: ${Registration_Tokens[${i}]},g" /tmp/values.yaml
	sed -i "s,tags: .*,tags: \"${tags[${i}]}\",g" /tmp/values.yaml
  microk8s.helm3 install ${RunnerName} --namespace gitlab-runner -f /tmp/values.yaml gitlab/gitlab-runner
done

