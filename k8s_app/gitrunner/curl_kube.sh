#!/bin/sh
############################################################################################
#
# holen des richtigen images
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
ARCH=$(apk --print-arch)
XLINK="amd64"
if [ "${ARCH} " == "aarch64 " ] 
then
	XLINK="arm64"  # Das ist der Raspberry
fi
echo "${ARCH} ${XLINK}"
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/${XLINK}/kubectl"
#

