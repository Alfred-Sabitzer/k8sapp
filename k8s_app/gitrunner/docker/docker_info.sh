#!/bin/sh
############################################################################################
#
# Warten bis der Docker bereit ist
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.

maximum=$(($1+0))  # Jetzt ist es eine Zahl
echo "Maximum $maximum"

count=1
dexit="check"

# Starten des Services im Hintergrund
/usr/local/bin/dockerd-entrypoint.sh > /dev/null 2>&1 &

# Warten bis der init mal läuft
while [ $dexit != "exit" ]
 do
   proc=$(ps -ef | grep -v "grep -i dockerd --host=unix:///var/run/docker.sock" | grep -i "dockerd --host=unix:///var/run/docker.sock")
   echo $proc
   if [ "$proc " != " " ] 
   then
     dexit="exit"
     ps -ef | grep -i "dockerd --host=unix:///var/run/docker.sock"
     echo " Exit -> Daemon läuft"
   else
   	sleep 10
   	count=$(($count+1))
   	echo "Check init $count $proc"
	if [ $count -gt $maximum ]
	then
	     dexit="exit"
	     echo " Exit -> Abbruch init $count" 
	     ps -ef | grep -i "dockerd --host=unix:///var/run/docker.sock"
	     exit 1
	fi
   fi
 done

echo $dexit

count=1
dexit="check"

# docker rennt?
while [ $dexit != "exit" ]
 do
   if ! docker info > /dev/null 2>&1; then
        echo "Docker läuft noch nicht!"
   	count=$(($count+1))
   	echo "Check Docker $count"
	if [ $count -gt $maximum ]
	then
	     dexit="exit"
	     echo " Exit -> Abbruch Docker check $count" 
	     docker info
	     exit 1
	fi
   else
     dexit="exit"
   fi
 done

echo $dexit
echo "Docker läuft - alles Gut"