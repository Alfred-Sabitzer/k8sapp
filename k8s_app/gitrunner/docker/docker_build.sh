#!/bin/sh
############################################################################################
# Erzeugen des Dockerfiles
############################################################################################
docker_registry="docker.registry:5000"
# Sonst kennt das Image den Host nicht
echo "192.168.0.213     docker.registry" >> /etc/hosts
image="${1}"
tag="latest"
# Docker bauen und in das remote Repo pushen
docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:${tag}
curl ${docker_registry}/v2/${image}/tags/list
#
