#!/bin/bash
############################################################################################
# Erzeugen des Dockerfiles
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.registry:5000"
image="cicd-docker"
datum=(`date '+%Y%m%d'`)
revision=(`date '+%H%M%S'`)
tag="${datum}-${revision}"
# Aktualisieren der Sourcen
git pull --ff-only
# Docker bauen und in das remote Repo pushen
docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:${tag}
docker build --force-rm . -t ${docker_registry}/${image}:latest
docker push ${docker_registry}/${image}:latest
curl ${docker_registry}/v2/${image}/tags/list
#
