#!/bin/sh
############################################################################################
#
# holen des richtigen images
#
############################################################################################
#
ARCH=$(apk --print-arch)
KubectlFile="kubectl_amd64"
if [ "${ARCH} " == "aarch64 " ]
then
	KubectlFile="kubectl_arm64"  # Das ist der Raspberry
fi
echo "${ARCH}  ${KubectlFile}"
chmod +x /usr/local/bin/${KubectlFile}
ln -s /usr/local/bin/${KubectlFile} /usr/local/bin/kubectl
#
