#!/bin/sh
############################################################################################
#
# holen des richtigen images
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
ARCH=$(apk --print-arch)
KptFile="kpt_linux_amd64"
if [ "${ARCH} " == "aarch64 " ] 
then
	KptFile="kpt_linux_arm64"  # Das ist der Raspberry
fi
echo "${ARCH}  ${KptFile}"
chmod +x /usr/local/bin/${KptFile}
ln -s /usr/local/bin/${KptFile} /usr/local/bin/kpt
#

