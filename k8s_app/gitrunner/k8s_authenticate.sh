#!/bin/bash
############################################################################################
#
# Certificate für den gitlab-runner. Dieses Skript muß im Cluster laufen (wegen der CA-Certificates)
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Siehe https://kubernetes.io/docs/reference/access-authn-authz/authentication/
#
domain="gitlab-runner"
password="gitlab-runner"
country="AT"
state="Steiermark"
locality="ZuHause"
organizationalunit="microk8s"
email="microk8s.raspberry@slainte.at"

#
#apiVersion: rbac.authorization.k8s.io/v1
#kind: ClusterRoleBinding
#metadata:
#  name: gitlab-runner-crgroup
#roleRef:
#  apiGroup: rbac.authorization.k8s.io
#  kind: ClusterRole
#  name: admin
#subjects:
#- apiGroup: rbac.authorization.k8s.io
#  kind: Group
#  name: gitlab:runner  
#---
#

organization="gitlab:runner" # Aus clusterrole-binding
commonname=$domain

#
# Jetzt brauchen wir die CA-Authority-keys vom mikroK8s in var/snap/microk8s/current/certs/
# Siehe https://microk8s.io/docs/services-and-ports
#
#First, this user must have a certificate issued by the Kubernetes cluster, and then present that certificate to the Kubernetes API. Create private key
pw=$(echo $RANDOM | md5sum | head -c 20)
openssl genrsa -out $domain.key -passout pass:$pw
#
# Generieren eines csr. Siehe https://kubernetes.io/docs/tasks/administer-cluster/certificates/
#
openssl req -new -key $domain.key \
            -nodes \
            -out $domain.csr \
            -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"

#openssl x509 -req -in $domain.csr -CA ca.crt -CAkey ca.key \
#-CAcreateserial -out $domain.crt -days 10000 \
#-extensions v3_ext -extfile csr.conf
#exit

#
# Siehe https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/
# Siehe https://www.frakkingsweet.com/adding-a-full-admin-user-in-kubernetes/
#
microk8s.kubectl delete certificatesigningrequests.certificates.k8s.io $domain 
#
b64=$(cat $domain.csr | base64 | tr -d "\n")
cat <<EOF | microk8s.kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: $domain
spec:
  groups:
  - system:authenticated
  - gitlab:runner
  request: $b64
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 315360000  # 10 Jahre
  usages:
  - digital signature
  - key encipherment
  - client auth  
EOF
#
microk8s.kubectl get csr
microk8s.kubectl certificate approve $domain
#kubectl get csr/$domain -o yaml
sleep 5s
microk8s.kubectl get csr $domain -o jsonpath='{.status.certificate}'| base64 -d > $domain.crt
cat  $domain.crt
#
exit

# Diese Statements erfolgen dann am Rechner des Users
kubectl config view
kubectl get all
kubectl config set-credentials $domain --client-key=$domain.key --client-certificate=$domain.crt --embed-certs=true
kubectl config set-context $domain --cluster=microk8s-cluster --user=$domain
kubectl config use-context $domain
kubectl config view
kubectl get all
kubectl config delete-user admin
kubectl config delete-context microk8s
kubectl config view
# 
