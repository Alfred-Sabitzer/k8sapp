---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${image}-depl
  namespace: ${namespace}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${image}-app      
  minReadySeconds: 60
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  template:
    metadata:
      labels:
        app: ${image}-app
        prometheus: k8s         
      annotations:
        sidecar.istio.io/inject: "false"
        prometheus.io/scrape: 'true'
        prometheus.io/port: '9091'
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - ${image}-app
            topologyKey: "kubernetes.io/hostname"      
      # Container Spec
      containers:
      - name: ${image}
        image: ${docker_registry}/${image}:${tag}
        imagePullPolicy: Always
        env:
        - name: image
          value: "${image}"
        - name: tag
          value: "${tag}"
        - name: TZ
          value: Europe/Vienna
        - name: LANG
          value: de_DE.UTF-8
        - name: LANGUAGE
          value: de_DE.UTF-8
        - name: LC_ALL
          value: de_DE.UTF-8          
        ports:
        - containerPort: 9091
          name: metrics
        resources:
          # requests ⇒ set minimum required resources when creating pods
          requests:
            # 250m ⇒ 0.25 CPU
            cpu: 125m
            memory: 32Mi
          # set maximum resorces
          limits:
            cpu: 250m
            memory: 64Mi        
        # check for lifetime liveness, restarts if dead
        livenessProbe:
          httpGet:
            path: /-/healthy
            port: 9091
            scheme: HTTP
          initialDelaySeconds: 10
          timeoutSeconds: 5
          periodSeconds: 59
          successThreshold: 1
          failureThreshold: 30   
        # check for initial readiness
        readinessProbe:
          httpGet:
            path: /-/ready
            port: 9091
            scheme: HTTP
          initialDelaySeconds: 10
          timeoutSeconds: 5
          periodSeconds: 37
          successThreshold: 1
          failureThreshold: 30   
      # Verhalten, beim Beenden
      terminationGracePeriodSeconds: 120
      restartPolicy: Always
---
