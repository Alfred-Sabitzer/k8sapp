#!/bin/bash
############################################################################################
# Löschen der temporären Dateien
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
sudo rm -f ./pushgateway_latest_container.tar.gz
sudo rm -Rf ./pushgateway_latest_container
