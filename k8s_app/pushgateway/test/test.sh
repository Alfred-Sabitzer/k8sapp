#!/bin/bash
############################################################################################
# Testmetriken
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

namespace="admin"
service="pushgateway-svc"

serviceIP=$(kubectl get -n ${namespace} service | grep -i ${service} | awk '{print $3 }')
if [ "${serviceIP} " = " " ] ;
then
	echo "FEHLER: ${service} im Namespace ${namespace} kann nicht gefunden werden! ${serviceIP}"
	exit -1
fi
cmd="kubectl -n ${namespace} port-forward service/${service} 9091:9091"

$cmd &
pid=$!
sleep 5

cat <<EOF | curl --data-binary @- http://localhost:9091/metrics/job/test/instance/test
# TYPE pushgateway_test counter
pushgateway_test{label="pushgateway"} 42
# TYPE pushgateway_2 gauge
# HELP pushgateway_2 Just an example.
pushgateway_2 2398.283
# HELP pushgateway_test_total Description of the metric
# TYPE pushgateway_test_total counter
# Comment that's not parsed by prometheus
pushgateway_test_total{method="post",code="400"}  3
# HELP pushgateway_test_request_duration_seconds request duration histogram
# TYPE pushgateway_test_request_duration_seconds histogram
pushgateway_test_request_duration_seconds_bucket{le="0.5"} 0
pushgateway_test_request_duration_seconds_bucket{le="1"} 1
pushgateway_test_request_duration_seconds_bucket{le="2"} 2
pushgateway_test_request_duration_seconds_bucket{le="3"} 3
pushgateway_test_request_duration_seconds_bucket{le="5"} 3
pushgateway_test_request_duration_seconds_bucket{le="+Inf"} 3
pushgateway_test_request_duration_seconds_sum 6
pushgateway_test_request_duration_seconds_count 3
# HELP pushgateway_test_duration_seconds A summary of the GC invocation durations.
# TYPE pushgateway_test_duration_seconds summary
pushgateway_test_duration_seconds{quantile="0"} 3.291e-05
pushgateway_test_duration_seconds{quantile="0.25"} 4.3849e-05
pushgateway_test_duration_seconds{quantile="0.5"} 6.2452e-05
pushgateway_test_duration_seconds{quantile="0.75"} 9.8154e-05
pushgateway_test_duration_seconds{quantile="1"} 0.011689149
pushgateway_test_duration_seconds_sum 3.451780079
pushgateway_test_duration_seconds_count 13118
EOF

curl -X GET http://localhost:9091/api/v1/status | jq

curl -X GET http://localhost:9091/api/v1/metrics | jq

kill ${pid}



