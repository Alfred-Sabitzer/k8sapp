ARG  BUILDER_IMAGE=golang:alpine
############################
# STEP 1 build executable binary
############################
FROM ${BUILDER_IMAGE} as builder

LABEL maintainer="The Prometheus Authors <prometheus-developers@googlegroups.com>"
LABEL URL="https://github.com/prometheus/pushgateway"

# timezone support
ENV TZ=Europe/Vienna

# Timezone support
RUN apk update \
    && apk --no-cache --update add build-base git curl tzdata && \
	cp /usr/share/zoneinfo/${TZ} /etc/localtime &&\
	echo $TZ > /etc/timezone

ARG ARCH="amd64"
ARG OS="linux"
ENV GOPATH="/go"

RUN mkdir -p /go/src &&\
    cd /go/src &&\
    git clone https://github.com/prometheus/pushgateway &&\
    cd /go/src/pushgateway &&\
    make --always-make --environment-overrides

############################
# STEP 2 build a small image
############################
#FROM scratch
FROM alpine:latest 
#-> Zum testen ist das ganz praktisch

LABEL maintainer="microk8s.raspberry@slainte.at"
LABEL Description="pushgateway"

RUN apk update \
    && apk --no-cache --update add curl 
   
# Import from builder.
#COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/localtime /etc/localtime
COPY --from=builder /etc/timezone /etc/timezone
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

COPY dummy.sh /
RUN chmod 755 /*.sh

COPY --chown=nobody:nobody --from=builder /go/src/pushgateway/pushgateway /bin/

USER nobody

EXPOSE 9091

# Starten mit den richtigen Vorarbeiten
#CMD ["/dummy.sh"]

ENTRYPOINT [ "/bin/pushgateway" ]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --interval=60s --timeout=10s CMD curl -k --silent --fail http://127.0.0.1:9091/-/healthy || exit 1
