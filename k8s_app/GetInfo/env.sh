#!/bin/bash
############################################################################################
#    $Date: 2021-10-30 09:46:30 +0200 (Sa, 30. Okt 2021) $
#    $Revision: 940 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/dev/make.sh $
#    $Id: make.sh 940 2021-10-30 07:46:30Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="getinfo"
export namespace="default slainte"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
