#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-12-05 10:56:16 +0100 (So, 05. Dez 2021) $"
echo "#    $Revision: 1536 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/test/test.sh $"
echo "#    $Id: test.sh 1536 2021-12-05 09:56:16Z alfred $"
echo "#"
echo "# Aufbereiten des Containers"
echo "#"
echo "############################################################################################"

echo "############### 301 #########################"
curl -k -v http://localhost
echo "############### pong #########################"
curl -k -v http://localhost/fpm-ping
echo "############### status #########################"
curl -k -v http://localhost/fpm-status
echo "############### status #########################"
curl -k -v http://localhost/nginx_status
echo "############### php-info #########################"
curl -k -v https://localhost/info.php
echo "############### portal #########################"
curl -k -v https://localhost
echo "########################################"

#
