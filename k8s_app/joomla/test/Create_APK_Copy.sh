#!/bin/bash
############################################################################################
#    $Date: 2021-12-16 12:31:53 +0100 (Do, 16. Dez 2021) $
#    $Revision: 1596 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/test/Create_APK_Copy.sh $
#    $Id: Create_APK_Copy.sh 1596 2021-12-16 11:31:53Z alfred $
#
# Erzeugen einer configmap aus einem File
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.

# Überprüfen der Parameter
if [ "${1} " = " " ] ;
then
	echo "Usage: $0 apk-file "
	echo "eg: $0	 apk.txt"
	exit -1
fi
shopt -o -s nounset #-No Variables without definition

ifile=${1}
if [ ! -f "${ifile}" ]; then # Das ist keine Datei
	echo "Usage: $0 apk-file "
	echo "\"$@\"" "ist keine Datei"
	exit -1
fi

echo " COPY --from=builder \ "
while read line
do
	if [[ ${line} == "#"* ]]; then # Kommentar
	  echo ${line}" \ " 
	elif [[ ${line} == *"contains:" ]]; then # Kommentar
	  echo "# "${line}" \ " 
	elif [[ ${line} == "" ]]; then # Leerzeile
	  echo "#"" \ " 
	else
	  echo " /"${line}" /"${line}" \ " 
	fi
done <  ${ifile}
#
