#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-12-16 20:57:27 +0100 (Do, 16. Dez 2021) $"
echo "#    $Revision: 1603 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/k8s.sh $"
echo "#    $Id: k8s.sh 1603 2021-12-16 19:57:27Z alfred $"
echo "#"
echo "# Check Live-Status (auch für andere Services) "
echo "#"
echo "############################################################################################"

export K8S=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT
export TOKEN=$(cat /var/run/secrets/tokens/${image}-token/token)
export CACERT=/var/run/secrets/tokens/${image}-token/ca.crt
export NAMESPACE=$(cat /var/run/secrets/tokens/${image}-token/namespace)

CRES=$(curl -s  -w "%{http_code}" -H "Authorization: Bearer $TOKEN" --cacert $CACERT $K8S/api/v1/namespaces/admin/services/pgadmin-svc | grep -i '"clusterIP":')
#echo $CRES
#"clusterIP": "10.152.183.201",
pf="\"clusterIP\": \""
sf="\","
xtext=${CRES#*"$pf"}
xtext=${xtext%"$sf"*}
#echo $xtext
res=$(curl -k -s -o /dev/null -w "%{http_code}" https://$xtext)
#echo ${res}
if [ "${res} " != "200 " ]; then # Datenbank ist nicht vorhanden.
    exit 1
fi
res=$(curl -k -s http://localhost/fpm-ping)
#echo ${res}
if [ "${res} " != "pong " ]; then # fpm und oder nginx nicht bereit
    exit 1
fi
#echo "Juchu"
exit 0 
#
