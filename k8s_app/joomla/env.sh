#!/bin/bash
############################################################################################
#    $Date: 2021-12-03 21:18:06 +0100 (Fr, 03. Dez 2021) $
#    $Revision: 1497 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/env.sh $
#    $Id: env.sh 1497 2021-12-03 20:18:06Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export image="joomla"
export namespace="slainte"
#Namespacespezifisch
. ../namespace/${namespace}_env.sh
#
