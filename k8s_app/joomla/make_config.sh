#!/bin/bash
echo "############################################################################################"
echo "#    $Date: 2021-12-04 16:25:17 +0100 (Sa, 04. Dez 2021) $"
echo "#    $Revision: 1529 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/make_config.sh $"
echo "#    $Id: make_config.sh 1529 2021-12-04 15:25:17Z alfred $"
echo "#"
echo "# Erzeugen der Konfig-Dateien"
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
/home/alfred/svn/trunk/k8s/dev/make_configmap_dir.sh ./config ./joomla_config.yaml
#
