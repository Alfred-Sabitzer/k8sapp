#!/bin/bash
############################################################################################
#    $Date: 2021-12-03 21:18:06 +0100 (Fr, 03. Dez 2021) $
#    $Revision: 1497 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/do.sh $
#    $Id: do.sh 1497 2021-12-03 20:18:06Z alfred $
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
svn update
source env.sh
joomla_dir="/home/alfred/Downloads/joomla/"
docker build --no-cache . -t ${image}:latest
#docker save ${image}:latest | gzip > ${image}_latest.tar.gz
docker kill $(docker ps | grep -i ${image}:latest | awk '{print $1 }') 
docker container rm -f $(docker container ls -a | grep -i ${image}_latest | awk '{print $1 }') 
docker run -d --name ${image}_latest -p 8080:80 -p 8443:443 -v ${joomla_dir}:/var/www/html/ ${image}:latest
sleep 10
docker ps
docker export $(docker ps | grep -i ${image}:latest | awk '{print $1 }') | gzip >  ${image}_latest_container.tar.gz
echo "docker exec -u 0 -it ${image}_latest /bin/sh"
#docker exec -u 0 -it ${image}_latest /bin/sh
#docker kill $(docker ps | grep -i ${image}:latest | awk '{print $1 }') 
#docker container rm $(docker container ls -a | awk '{print $1 }') 
#docker run -it --name ${image}_latest ${image}:latest /bin/bash
#

