---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${image}-depl
  namespace: ${namespace}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${image}-app
  minReadySeconds: 60
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  template:
    metadata:
      labels:
        app: ${image}-app
      annotations:
        sidecar.istio.io/inject: "false"
    spec:
      serviceAccountName: ${image}-sa            
      securityContext:
        #runAsNonRoot: true
        #runAsUser: 1000
        fsGroup: 82    
      # Verhalten, wenn mehrere Replicas notwendig sind
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - ${image}-app
            topologyKey: "kubernetes.io/hostname"      
      # Mounted Volumes
      volumes:
        - name: ${image}-joomla
          persistentVolumeClaim:
            claimName: ${image}-pvc      
        - name: certificate-volume
          secret:
            secretName: ${secretName}
        - name: joomla-volume
          secret:
            secretName: joomla
        - name: config-volume
          configMap:
            name: joomla-config-cm
        - name: ${image}-token
          secret:
            secretName: ${image}-token
      # Container Spec
      containers:
      - name: ${image}
        image: ${docker_registry}/${image}:${tag}
        imagePullPolicy: Always
        env:
        - name: image
          value: "${image}"
        - name: tag
          value: "${tag}"
        - name: TZ
          value: Europe/Vienna
        - name: LANG
          value: de_DE.UTF-8
        - name: LANGUAGE
          value: de_DE.UTF-8
        - name: LC_ALL
          value: de_DE.UTF-8          
        ports:
        - containerPort: 80
          name: http
        - containerPort: 443
          name: https
        volumeMounts:
        - mountPath: /var/www/html/
          name: ${image}-joomla
        - name: certificate-volume
          mountPath: /ssl/
          readOnly: true
        - name: config-volume
          mountPath: /config/
          readOnly: true
        - name: joomla-volume
          mountPath: /joomla_db/
          readOnly: true
        - name: ${image}-token
          mountPath: /var/run/secrets/tokens/${image}-token/
          readOnly: true
        # Definition der Ressourcen-Limits
        # You must specify requests for CPU to autoscale based on CPU utilization
        resources:
          # requests ⇒ set minimum required resources when creating pods
          requests:
            # 250m ⇒ 0.25 CPU
            cpu: 250m
            memory: 128Mi
          # set maximum resorces
          limits:
            cpu: 500m
            memory: 512Mi        
        # check for lifetime liveness, restarts if dead
        livenessProbe:
          exec:
            command: ["/k8s.sh"]
          initialDelaySeconds: 10
          timeoutSeconds: 5
          periodSeconds: 31
          successThreshold: 1
          failureThreshold: 3
        startupProbe:
          exec:
            command: ["/k8s.sh"]
          initialDelaySeconds: 60
          timeoutSeconds: 5
          periodSeconds: 31
          successThreshold: 1
          failureThreshold: 30
        # check for initial readiness
        readinessProbe:
          httpGet:
            path: /fpm-ping
            port: 80
            scheme: HTTP
          initialDelaySeconds: 30
          timeoutSeconds: 5
          periodSeconds: 59
          successThreshold: 1
          failureThreshold: 30   
      # Verhalten, beim Beenden
      terminationGracePeriodSeconds: 120
      restartPolicy: Always
#      dnsPolicy: ClusterFirst          
#      hostNetwork: true
#      dnsPolicy: ClusterFirstWithHostNet      
---
