#!/bin/sh
echo "############################################################################################"
echo "#    $Date: 2021-12-16 19:01:46 +0100 (Do, 16. Dez 2021) $"
echo "#    $Revision: 1602 $"
echo "#    $Author: alfred $"
echo "#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s_app/joomla/start.sh $"
echo "#    $Id: start.sh 1602 2021-12-16 18:01:46Z alfred $"
echo "#"
echo "# Aufbereiten des Containers"
echo "#"
echo "############################################################################################"

# Make sure files/folders needed by the processes are accessable when they run under the www-data user
chown -R www-data:www-data /var/www/html
chown -R www-data:www-data /run 
chown -R www-data:www-data /var/run
chown -R www-data:www-data /var/lib/nginx
chown -R www-data:www-data /var/log/nginx
chown -R www-data:www-data /etc/nginx

# php.ini 
cp -v /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
#sed -i 's,;extension=pgsql,extension=pgsql,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=pdo_pgsql,extension=pdo_pgsql,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=gd,extension=gd,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=intl,extension=intl,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=ldap,extension=ldap,g' /usr/local/etc/php/php.ini
sed -i 's,;gd.jpeg_ignore_warning = 1,gd.jpeg_ignore_warning = 1,g' /usr/local/etc/php/php.ini
sed -i 's,output_buffering =.*,output_buffering = off,g' /usr/local/etc/php/php.ini
sed -i 's,;upload_tmp_dir =.*,upload_tmp_dir = /var/www/html/upload_tmp_dir,g' /usr/local/etc/php/php.ini
sed -i 's,upload_max_filesize =.*,upload_max_filesize = 20M,g' /usr/local/etc/php/php.ini
sed -i 's,post_max_size =.*,post_max_size = 60M,g' /usr/local/etc/php/php.ini
#
mkdir -p /var/www/html/upload_tmp_dir
# Kopieren der Zertifikate und der Config
cp -v -f /config/* /etc/nginx/
rm -f /etc/nginx/conf.d/default.conf
cp -v -f /ssl/* /etc/nginx/ssl/
cp -v /etc/php7/php-fpm.conf /usr/local/etc/php-fpm.conf 
cp -v -f /etc/nginx/fpm-pool.conf /usr/local/etc/php-fpm.d/www.conf
cp -v -f /etc/nginx/php.ini /usr/local/etc/php/conf.d/custom.ini
cp -v -f /etc/nginx/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
cp -v -f /etc/nginx/php.ini /etc/php7/conf.d/custom.ini
cp -v -f /etc/nginx/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Socket erzeugen
touch /var/run/php/php-fpm.sock
chown -R www-data:www-data /var/run/php
chown www-data:www-data /var/run/php/php-fpm.sock
chmod 766 /var/run/php/php-fpm.sock

# Rechte richtig setzen
chown -R www-data:www-data /etc/php7
chown -R www-data:www-data /etc/nginx
chown www-data:www-data /var/run/php/php-fpm.sock
chown www-data:www-data /var/www

chmod 755 /var/lib/nginx
chmod 755 /var/www/html

# Prüfen ob Erstinstallation
DIR="/var/www/html"
FILE="joomla_installation_done.html"
if [ -d "$DIR" ]
then
	if [ ! -f $DIR/$FILE ]; then
	    echo "$DIR/$FILE existiert nicht. Neuinstallation"
	    tar -xz -f /joomla/*.tar.gz --overwrite --directory=$DIR
	    chown -R www-data:www-data *
	    chown -R www-data:www-data *.*
	    touch $DIR/$FILE
	    cat <<EOF >> $DIR/$FILE
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
     "http://www.w3.org/TR/html4/transitional.dtd">
<html>
<head>
  <meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
  <title>Joomla Installation Report</title>
</head>
<body>
<center>
EOF
	    ls -lisa >> $DIR/$FILE
	    cat <<EOF >> $DIR/$FILE
</center>
</body>
</html>
EOF
	    echo "$DIR/$FILE erstellt"
	fi
else
	echo "Verzeichnis $DIR nicht gefunden."
	exit -1
fi

cp -R -v -f /html/ $DIR/../
echo "Suchen nach Files mit falschem Owner"
for f in $(find $DIR -type f \! -user www-data ) ; do 
	chown -v www-data:www-data ''${f}''
done
echo "Suchen nach Verzeichnissen mit falschem Owner"
for f in $(find $DIR -type d \! -user www-data ) ; do 
	chown -v www-data:www-data ''${f}''
done
echo "$DIR/$FILE existiert. Setzen der Passwörter"

# Setzen der Variablen
export JOOMLA_DB_USER=$(cat /joomla_db/username)
export JOOMLA_DB_PASSWORD=$(cat /joomla_db/password)
sed -i "s,public \$user =.*,public \$user = '${JOOMLA_DB_USER}';,g" /var/www/html/configuration.php
sed -i "s,public \$password =.*,public \$password = '${JOOMLA_DB_PASSWORD}';,g" /var/www/html/configuration.php

export K8S=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT
export TOKEN=$(cat /var/run/secrets/tokens/${image}-token/token)
export CACERT=/var/run/secrets/tokens/${image}-token/ca.crt
export NAMESPACE=$(cat /var/run/secrets/tokens/${image}-token/namespace)
CRES=$(curl -s  -w "%{http_code}" -H "Authorization: Bearer $TOKEN" --cacert $CACERT $K8S/api/v1/namespaces/admin/services/pgadmin-svc | grep -i '"clusterIP":')
#echo $CRES
#"clusterIP": "10.152.183.201",
pf="\"clusterIP\": \""
sf="\","
xtext=${CRES#*"$pf"}
xtext=${xtext%"$sf"*}
sed -i "s,public \$host =.*,public \$host = '${xtext}';,g" /var/www/html/configuration.php
	
# Starten des Supervisors
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
