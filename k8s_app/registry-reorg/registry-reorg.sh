#!/bin/sh
############################################################################################
# Reorganisieren der microk8s.registry
############################################################################################

echo $*
date
ENV_REORG=${1}
echo "Purge-Value:"${ENV_REORG}
if [ "${ENV_REORG} " = " " ] ;
 then
   ENV_REORG="3"
fi

echo "192.168.0.213   docker.registry" >> /etc/hosts
docker_registry="docker.registry:5000"

# Anzeige der Repository-Einträge
repositories=$(curl -s ${docker_registry}/v2/_catalog)
echo ${repositories}
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo ${repo}
  tags=$(curl -s "http://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in ${tags}; do
    echo "          "${tag}
  done
done
#
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo ${repo}
  rm -f /tmp/${repo}.txt
  touch /tmp/${repo}.txt
  tags=$(curl -s "http://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in ${tags}; do
	  if [ "${tag} " != "latest " ] ;
	  then
      echo ${tag} >> /tmp/${repo}.txt
    fi
  done
# Die ältesten löschen (bis auf 3)
  sort -f -r /tmp/${repo}.txt > /tmp/${repo}.sort
  tail -n +${ENV_REORG}  /tmp/${repo}.sort > /tmp/${repo}.tail
  sort -f  /tmp/${repo}.tail > /tmp/${repo}.delete
# Löschen der Einträge
  tags=$(cat /tmp/${repo}.delete)
  for tag in ${tags}; do
    echo "Lösche      "${repo}:${tag}
    curl -v -k -X DELETE "http://${docker_registry}/v2/${repo}/manifests/$(
          curl -k -s -I \
              -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
              "http://${docker_registry}/v2/${repo}/manifests/${tag}" \
          | awk '$1 == "Docker-Content-Digest:" { print $2 }' \
          | tr -d $'\r' \
        )"
  done
done
# Anzeige der verbleibenden Einträge
echo ${repositories}
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo ${repo}
  tags=$(curl -s "http://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in ${tags}; do
    echo "          "${tag}
  done
done