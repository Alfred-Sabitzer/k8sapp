#!/bin/bash
############################################################################################
#
# Bauen und deployen
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.registry:5000"
git pull --ff-only
image="registry-reorg"
tag="latest"
# Docker bauen und in das remote Repo pushen
docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:${tag}
curl ${docker_registry}/v2/${image}/tags/list
#