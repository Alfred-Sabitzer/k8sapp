#!/bin/bash
############################################################################################
#
# Installieren des Servicemesh linkerd
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
revision="latest"
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/${revision}"
id="/opt/cluster/${app}/${xd}/${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
cat <<EOF > ${wd}/do_linkerd.sh
#!/bin/bash
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
microk8s enable linkerd
sleep 30s
microk8s linkerd check

EOF
chmod 755 ${wd}/do_linkerd.sh
#
ansible pc1 -m shell -a ${id}'/do_linkerd.sh ' > ${wd}'/do_linkerd.log'
#
cat <<EOF > ${wd}/linkerd.yaml
---
# Zugang mit admin/admin
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: web-ingress-auth
  namespace: linkerd-viz
data:
  auth: YWRtaW46JGFwcjEkbjdDdTZnSGwkRTQ3b2dmN0NPOE5SWWpFakJPa1dNLgoK
---
# apiVersion: networking.k8s.io/v1beta1 # for k8s < v1.19
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: web-ingress
  namespace: linkerd-viz
  annotations:
    kubernetes.io/ingress.class: public
    cert-manager.io/cluster-issuer: letsencrypt-prod
    nginx.ingress.kubernetes.io/upstream-vhost: $service_name.$namespace.svc.cluster.local:8084
    nginx.ingress.kubernetes.io/configuration-snippet: |
      proxy_set_header Origin "";
      proxy_hide_header l5d-remote-ip;
      proxy_hide_header l5d-server-id;
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: web-ingress-auth
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required'
spec:
  tls:
    - hosts:
        - viz.k8s.slainte.at
      secretName: viz.k8s-slainte-at-tls
  rules:
    - host: viz.k8s.slainte.at
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: web
                port:
                  number: 8084
EOF
#
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/linkerd.yaml'
#