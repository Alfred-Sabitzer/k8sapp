#!/bin/bash
############################################################################################
#    $Date: 2021-10-20 21:10:57 +0200 (Mi, 20. Okt 2021) $
#    $Revision: 643 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K4_ingress.sh $
#    $Id: K4_ingress.sh 643 2021-10-20 19:10:57Z alfred $
#
# Schnell-Installation microk8s - Installation praktischer AddOns
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 643 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
ansible pc1 -m shell -a 'microk8s enable metrics-server'
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc1 -m shell -a 'microk8s enable ingress'
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc1 -m shell -a 'microk8s enable dns'
ansible pc -m shell -a 'microk8s status --wait-ready'
##
## Jetzt ist der Cluster online und verfügbar
##
