#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
${indir}/K1.sh
${indir}/K1_bash_aliases.sh
${indir}/K1_cluster.sh
${indir}/K2_helm.sh
${indir}/K3_metallb.sh
${indir}/K4_ingress.sh
${indir}/K5_certmanager.sh
#${indir}/K6_rancher.sh
${indir}/K7_longhorn.sh
#${indir}/K7_openebs.sh Funktioniert noch nicht
${indir}/K8_modifications.sh
${indir}/K10_Dashboard.sh
${indir}/K11_Prometheus.sh
${indir}/K12_local.sh
${indir}/K13_registry.sh
${indir}/K14_webserver.sh
${indir}/K15_ntp.sh
${indir}/K16_Prometheus_Grafana.sh
${indir}/K17_linkerd.sh
#
# Nun ist der Cluster bereit
##


