#!/bin/bash
############################################################################################
#    $Date: 2021-12-01 09:25:22 +0100 (Mi, 01. Dez 2021) $
#    $Revision: 1445 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K13_registry.sh $
#    $Id: K13_registry.sh 1445 2021-12-01 08:25:22Z alfred $
#
# Schnell-Installation microk8s - Installation praktischer AddOns
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 1445 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
# Zu diesem Zeitpunkt sollte es eine Default-Storage-Class geben, wo das abgelegt wird.
ansible pc1 -m shell -a 'microk8s enable registry:size=40Gi'
ansible pc -m shell -a 'microk8s status --wait-ready'
#
# Adaptieren der Services
#
cat <<EOF > ${wd}/do_registry.sh
#!/bin/bash
#
#    \$Date: 2021-12-01 09:25:22 +0100 (Mi, 01. Dez 2021) $
#    \$Revision: 1445 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K13_registry.sh $
#    \$Id: K13_registry.sh 1445 2021-12-01 08:25:22Z alfred $
#
# Ändern des Services auf Loadbalancer
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# registry - 192.168.0.213
microk8s kubectl -n container-registry get service registry -o yaml > ${id}/registry-svc.yaml
sed 's/NodePort/LoadBalancer/' ${id}/registry-svc.yaml > ${id}/new-registry-svc.yaml
microk8s kubectl apply -f ${id}/new-registry-svc.yaml --validate=false
EOF
#
chmod 755 ${wd}/do_registry.sh
ansible pc1 -m shell -a ${id}'/do_registry.sh ' > ${wd}'/do_registry.log'
#
cat <<EOF > ${wd}/do_nodes.sh
#!/bin/bash
#
#    \$Date: 2021-12-01 09:25:22 +0100 (Mi, 01. Dez 2021) $
#    \$Revision: 1445 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K13_registry.sh $
#    \$Id: K13_registry.sh 1445 2021-12-01 08:25:22Z alfred $
#
# Eintragen der Nodes in die hosts-Datei
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

sudo sed --in-place '/docker.registry/d' /etc/hosts
microk8s kubectl -n container-registry get service registry -o yaml > ${id}/nodes-registry-svc.yaml
ip=\$(cat ${id}/nodes-registry-svc.yaml  | grep -i " ip: " | awk '{print \$3 }')
text="\${ip}     docker.registry"
sudo sed -i "$ a \${text}" /etc/hosts
#
EOF
#
chmod 755 ${wd}/do_nodes.sh
ansible pc -m shell -a ${id}'/do_nodes.sh '
#
# Und jetzt die Repository-Info
#
cat <<EOF > ${wd}/do_docker.sh
#!/bin/bash
#
#    \$Date: 2021-12-01 09:25:22 +0100 (Mi, 01. Dez 2021) $
#    \$Revision: 1445 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K13_registry.sh $
#    \$Id: K13_registry.sh 1445 2021-12-01 08:25:22Z alfred $
#
# Eintragen des Endpoints in die Docker-Registry
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

FILENAME="/var/snap/microk8s/current/args/containerd-template.toml"

sudo sed --in-place '/docker.registry:5000/d' \${FILENAME}
text='      [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.registry:5000"]'
echo  "\${text}" | tee -a \${FILENAME}
text='        endpoint = ["http://docker.registry:5000"]'
echo  "\${text}" | tee -a \${FILENAME}
EOF
#
chmod 755 ${wd}/do_docker.sh
ansible pc -m shell -a ${id}'/do_docker.sh '
#
# Und jetzt für den Docker selbst
#
cat <<EOF > ${wd}/do_pull.sh
#!/bin/bash
#
#    \$Date: 2021-12-01 09:25:22 +0100 (Mi, 01. Dez 2021) $
#    \$Revision: 1445 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K13_registry.sh $
#    \$Id: K13_registry.sh 1445 2021-12-01 08:25:22Z alfred $
#
# Konfiguration damit das docker-pull funktioniert
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

sudo mkdir -p /etc/docker
sudo rm -f /etc/docker/daemon.json
tfile=\$(mktemp /tmp/daemon.XXXXXXXXX)

sudo cat <<AllOver > \${tfile}
{
    "insecure-registries" : ["docker.registry:5000", "localhost:32000"] 
}
AllOver
sudo cp -f \${tfile} /etc/docker/daemon.json
sudo chmod 666 /etc/docker/daemon.json
#
EOF
#
chmod 755 ${wd}/do_pull.sh
ansible pc -m shell -a ${id}'/do_pull.sh '

##
## Jetzt ist die Docker-Registry online und verfügbar
##
