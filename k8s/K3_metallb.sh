#!/bin/bash
############################################################################################
#    $Date: 2021-05-31 14:24:06 +0200 (Mo, 31. Mai 2021) $
#    $Revision: 528 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K3_metallb.sh $
#    $Id: K3_metallb.sh 528 2021-05-31 12:24:06Z alfred $
#
# Schnell-Installation microk8s - metallb als Loadbalancer
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 528 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
ansible pc1 -m shell -a 'microk8s enable metallb:192.168.0.210-192.168.0.244'
ansible pc -m shell -a 'microk8s status --wait-ready'
##

