#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Installation der Serviceaccounts
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
revision="latest"
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
# stellt den NTP-Server auf Monitoring
ansible pc -m shell -a 'sudo sed --in-place ''/ntp/d'' /etc/hosts'
ansible pc -m shell -a 'sudo echo  "192.168.0.2     ntp" | sudo tee -a /etc/hosts'
# Richtige Settings
ansible pc -m shell -a 'sudo sed --in-place ''/NTP=/d'' /etc/systemd/timesyncd.conf '
ansible pc -m shell -a 'sudo echo  "NTP=ntp" | sudo tee -a /etc/systemd/timesyncd.conf '
ansible pc -m shell -a 'sudo sed --in-place ''/FallbackNTP=ntp.ubuntu.com/d'' /etc/systemd/timesyncd.conf '
ansible pc -m shell -a 'sudo echo  "FallbackNTP=ntp.ubuntu.com" | sudo tee -a /etc/systemd/timesyncd.conf '
# Restart des Services
ansible pc -m shell -a 'sudo systemctl stop systemd-timesyncd.service'
ansible pc -m shell -a 'sudo systemctl start systemd-timesyncd.service'
ansible pc -m shell -a 'sudo systemctl status systemd-timesyncd.service'
#
ansible pc -m shell -a 'sudo microk8s stop '
ansible pc -m shell -a 'microk8s status --wait-ready'
#
# Logischer Link (kann für alle gut sein).
#
ansible pc -m shell -a 'sudo rm -f /usr/bin/kubectl '
ansible pc -m shell -a 'sudo ln -s /snap/microk8s/current/kubectl /usr/bin/kubectl '
#
# Startup-file für Modprobe
#
cat <<EOF > ${wd}/loop_gluster.service
#
# Servicedefinition für ModProbe
#
[Unit]
Description=modprobe für eh alles. Das tut niemals weh.
DefaultDependencies=false
Before=local-fs.target
After=systemd-udev-settle.service
Requires=systemd-udev-settle.service

[Service]
Type=oneshot
ExecStart=/bin/bash -c "modprobe dm_thin_pool && modprobe dm_snapshot && modprobe dm_mirror && modprobe fuse "

[Install]
WantedBy=local-fs.target
EOF
#
ansible pc -m shell -a 'sudo cp -f '${id}'/loop_gluster.service /etc/systemd/system/loop_gluster.service'
ansible pc -m shell -a 'sudo chown root:root /etc/systemd/system/loop_gluster.service'
ansible pc -m shell -a 'sudo chmod 755 /etc/systemd/system/loop_gluster.service'
ansible pc -m shell -a 'ls -lisa /etc/systemd/system/loop_gluster.service'
ansible pc -m shell -a 'sudo systemctl enable /etc/systemd/system/loop_gluster.service'
#
ansible pc -m shell -a 'sudo apt-get update'
ansible pc -m shell -a 'sudo apt-get upgrade -y'
# Prüfen der Files auf Konsistenz
ansible pc -m shell -a 'sudo apt-get -y install open-iscsi'
ansible pc -m shell -a 'sudo systemctl enable iscsid'
ansible pc -m shell -a 'sudo systemctl start iscsid'
ansible pc -m shell -a 'sudo systemctl status iscsid'
ansible pc -m shell -a 'sudo apt-get install -y mc sshfs tree'
ansible pc -m shell -a 'sudo apt-get install bash-completion -y'
# Jetzt kommt microk8s
ansible pc -m shell -a 'sudo microk8s stop '
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'sudo snap remove microk8s '
ansible pc -m shell -a 'sudo rm -rf /var/snap/microk8s'
ansible pc -m shell -a 'sudo rm -rf /home/alfred/.kube '
ansible pc -m shell -a 'sudo rm -rf /home/ansible/.kube '
ansible pc -m shell -a 'sudo snap install microk8s --classic --channel=1.22/stable'
ansible pc -m shell -a 'sudo snap info microk8s | grep -i tracking'
ansible pc -m shell -a 'sudo usermod -a -G microk8s alfred'
ansible pc -m shell -a 'sudo usermod -a -G microk8s ansible'
ansible pc -m shell -a 'sudo shutdown -r now'
#
sleep 2m
#
ansible-playbook -v ${HOME}/check_hosts.yaml
#ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'microk8s start'
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'microk8s inspect'
ansible pc -m shell -a 'microk8s enable storage dns rbac ha-cluster'
ansible pc -m shell -a 'microk8s status --wait-ready'
#
# Nun müssen händisch die Nodes zu einem cluster verbunden werden
# microk8s add-node
#


