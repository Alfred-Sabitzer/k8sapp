#!/bin/bash
############################################################################################
#    $Date: 2021-10-20 22:03:50 +0200 (Mi, 20. Okt 2021) $
#    $Revision: 648 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K7_openebs.sh $
#    $Id: K7_openebs.sh 648 2021-10-20 20:03:50Z alfred $
#
# Installation openebs
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 648 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
# initialisieren der fstab - entfernen eventueller Versuche
cat <<EOF > ${wd}/fstab
LABEL=writable	/	 ext4	defaults	0 0
LABEL=system-boot       /boot/firmware  vfat    defaults        0       1
ansible@monitoring:/home/ansible/copy /opt/cluster fuse.sshfs rw,delay_connect,idmap=user,uid=0,gid=0,umask=0,allow_other,_netdev,workaround=rename 0 0
EOF
ansible pc -m shell -a 'sudo cp -f '${id}'/fstab /etc/fstab'
ansible pc -m shell -a 'sudo cat /etc/fstab'
ansible pc -m shell -a 'sudo shutdown -r now'
#
sleep 2m
#
ansible-playbook -v ${HOME}/check_hosts.yaml
## Jetzt kommt die SSD's
ansible pc -m shell -a 'sudo wipefs -a /dev/sda'
ansible pc -m shell -a 'sudo mkfs.ext4 /dev/sda'
ansible pc -m shell -a 'sudo rm -f -R /var/data/openebs' 
ansible pc -m shell -a 'sudo mkdir /var/data/openebs'
ansible pc -m shell -a 'sudo mount /dev/sda /var/data/openebs'
ansible pc -m shell -a 'df -h | grep -i sda'
# die Verbindung über ssh muß händisch gemacht werden.
# Hinzufügen der SSD-Disks
ansible pc -m shell -a 'sudo printf $(sudo blkid -o export /dev/sda | grep UUID)" /var/data/openebs       ext4    defaults        0       2" | sudo tee -a /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
ansible pc -m shell -a 'sudo apt-get update'
ansible pc -m shell -a 'sudo apt-get upgrade -y'
ansible pc -m shell -a 'sudo apt-get -y install open-iscsi'
ansible pc -m shell -a 'sudo systemctl enable iscsid'
ansible pc -m shell -a 'sudo systemctl start iscsid'
ansible pc -m shell -a 'sudo systemctl status iscsid'
# https://longhorn.io/docs/1.1.1/deploy/install/
ansible pc -m shell -a 'sudo apt-get install curl util-linux jq nfs-common -y '
#
ansible pc -m shell -a 'microk8s start '
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc1 -m shell -a 'microk8s enable openebs '
#
cat <<EOF > ${wd}/openebs-configmap.yaml
#  \$Date: 2021-10-20 22:03:50 +0200 (Mi, 20. Okt 2021) $
#  \$Revision: 648 $
#  \$Author: alfred $
#  \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K7_openebs.sh $
#  \$Id: K7_openebs.sh 648 2021-10-20 20:03:50Z alfred $
# Adaptierte Konfigmap für die ssd's
#
# Spezialisierte Config-map
apiVersion: v1
data:
  node-disk-manager.config: |
    probeconfigs:
      - key: udev-probe
        name: udev probe
        state: true
      - key: seachest-probe
        name: seachest probe
        state: false
      - key: smart-probe
        name: smart probe
        state: true
    filterconfigs:
      - key: os-disk-exclude-filter
        name: os disk exclude filter
        state: true
        exclude: "/,/etc/hosts,/boot"
      - key: vendor-filter
        name: vendor filter
        state: true
        include: ""
        exclude: "CLOUDBYT,OpenEBS"
      - key: path-filter
        name: path filter
        state: true
        # Hier sind die SSD's inkludiert
        include: "/dev/sda"
        # Und die Boot-Platte wird ausgeschlossen
        exclude: "/dev/mmcblk,loop,fd0,sr0,/dev/ram,/dev/dm-,/dev/md,/dev/rbd,/dev/zd"
kind: ConfigMap
metadata:
  annotations:
    meta.helm.sh/release-name: openebs
    meta.helm.sh/release-namespace: openebs
  labels:
    app.kubernetes.io/managed-by: Helm
  name: openebs-ndm-config
  namespace: openebs
EOF
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/openebs-configmap.yaml'
#
