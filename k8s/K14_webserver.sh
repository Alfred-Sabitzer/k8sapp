#!/bin/bash
############################################################################################
#    $Date: 2021-11-28 11:05:45 +0100 (So, 28. Nov 2021) $
#    $Revision: 1404 $apiVersion: networking.k8s.io/v1
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K14_webserver.sh $
#    $Id: K14_webserver.sh 1404 2021-11-28 10:05:45Z alfred $
#
# Einspielen der lokalen Konfigurationen - Produktiv ist slainte.
# https://stackoverflow.com/questions/67430592/how-to-setup-letsencrypt-with-kubernetes-microk8s-using-default-ingress
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 1404 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#

cat <<EOF > ${wd}/webserver-depl-svc.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webserver-depl
  namespace: slainte
spec:
  selector:
    matchLabels:
      app: webserver-app
  template:
    metadata:
      labels:
        app: webserver-app
    spec:
      containers:
        - name: webserver-app
          image: nginx:1.20
---
apiVersion: v1
kind: Service
metadata:
  name: webserver-svc
  namespace: slainte
spec:
  selector:
    app: webserver-app
  ports:
  - port: 80
    name: http
    targetPort: 80
    protocol: TCP    
  - port: 443
    name: https
    targetPort: 443
    protocol: TCP    
EOF
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/webserver-depl-svc.yaml'

cat <<EOF > ${wd}/letsencrypt-staging.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-staging
spec:
  acme:
#change to your email
    email: slainte@slainte.at
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: letsencrypt-staging
    solvers:
    - http01:
        ingress:
          class: public
EOF
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/letsencrypt-staging.yaml'

cat <<EOF > ${wd}/letsencrypt-prod.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
#change to your email
    email: slainte@slainte.at
    privateKeySecretRef:
       name: letsencrypt-prod
    solvers:
    - http01:
        ingress:
          class: public
EOF
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/letsencrypt-prod.yaml'

cat <<EOF > ${wd}/ingress-routes-update.yaml 
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: webserver-routes
  namespace: slainte
  annotations:
  # Class checken mit kubectl -n ingress describe daemonset.apps/nginx-ingress-microk8s-controller
    kubernetes.io/ingress.class: public
# Das ist für das Zertifikat
    cert-manager.io/cluster-issuer: "letsencrypt-prod"    
# Das ist für das http -> https forwarding
# See https://kubernetes.github.io/ingress-nginx/examples/rewrite/
    nginx.ingress.kubernetes.io/rewrite-target: /\$1
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-temporary-redirect: "false"
    nginx.ingress.kubernetes.io/secure-backends: "true"
    nginx.ingress.kubernetes.io/ssl-proxy-headers: "X-Forwarded-Proto: https"
    nginx.ingress.kubernetes.io/proxy-body-size: 0m
    nginx.ingress.kubernetes.io/proxy-buffering: "off"
#    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
# https://github.com/nginxinc/kubernetes-ingress/tree/v1.12.0/examples/ssl-services
#    nginx.ingress.kubernetes.io/ssl-services: "\${image}-svc"
#    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS" 
spec:
  tls:
  - hosts:
    - k8s.slainte.at
    secretName: k8s-slainte-at-tls
  rules:
  - host: k8s.slainte.at
    http:
      paths:
      - path: /(.*)
        pathType: Prefix
        backend:
          service: 
            name: webserver-svc
            port: 
              number: 80
  defaultBackend:
    service:
      name: webserver-svc
      port:
        number: 80              
EOF
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/ingress-routes-update.yaml '

# Service PROD
curl -k -v http://k8s.slainte.at
#erreichbar sein.
#Aber auch mit https
curl -k -v https://k8s.slainte.at
#
## Prüfen des Zertifikates
ansible pc1 -m shell -a 'microk8s kubectl get certificate --all-namespaces'
ansible pc1 -m shell -a 'microk8s kubectl describe certificate --all-namespaces'
ansible pc1 -m shell -a 'microk8s kubectl get certificaterequests.cert-manager.io '
ansible pc1 -m shell -a 'microk8s kubectl describe certificaterequests '
ansible pc1 -m shell -a 'microk8s kubectl get certificatesigningrequests.certificates.k8s.io '
ansible pc1 -m shell -a 'microk8s kubectl get Issuer'
ansible pc1 -m shell -a 'microk8s kubectl get ClusterIssuer'
ansible pc1 -m shell -a 'microk8s kubectl describe ClusterIssuer letsencrypt-prod '
ansible pc1 -m shell -a 'microk8s kubectl get challenges.acme.cert-manager.io '
ansible pc1 -m shell -a 'microk8s kubectl describe challenges.acme.cert-manager.io '
##

exit

