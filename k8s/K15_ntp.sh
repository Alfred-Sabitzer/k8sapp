#!/bin/bash
############################################################################################
#    $Date: 2021-12-11 18:17:10 +0100 (Sa, 11. Dez 2021) $
#    $Revision: 1573 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K15_ntp.sh $
#    $Id: K15_ntp.sh 1573 2021-12-11 17:17:10Z alfred $
#
# Schnell-Installation microk8s - Installation praktischer AddOns
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 1573 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
cat <<EOF > ${wd}/do_ntp.sh
#!/bin/bash
#
#    \$Date: 2021-12-11 18:17:10 +0100 (Sa, 11. Dez 2021) $
#    \$Revision: 1573 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K15_ntp.sh $
#    \$Id: K15_ntp.sh 1573 2021-12-11 17:17:10Z alfred $
#
# Eintragen der ntp-Server-Adresse
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

FILENAME="/etc/hosts"

sed --in-place '/ntp/d' \${FILENAME}
# Vorerst lassen wir den ntp-server ausserhalb des clusters, Macht sonst Probleme beim Cluster-Restart
#text='192.168.0.240     ntp'
text='192.168.0.2     ntp'
echo  "\${text}" | tee -a \${FILENAME}
sudo systemctl stop systemd-timesyncd.service
sudo systemctl start systemd-timesyncd.service
sudo systemctl status systemd-timesyncd.service

EOF
#
chmod 755 ${wd}/do_ntp.sh
ansible pc -m shell -a 'sudo '${id}'/do_ntp.sh '
#
