#!/bin/bash
############################################################################################
#    $Date: 2021-12-19 20:48:43 +0100 (So, 19. Dez 2021) $
#    $Revision: 1662 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K12_local.sh $
#    $Id: K12_local.sh 1662 2021-12-19 19:48:43Z alfred $
#
# Adaption der Service-IP's und Storageklassen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 1662 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
cat <<EOF > ${wd}/namespace-slainte.yaml
#  \$Date: 2021-12-19 20:48:43 +0100 (So, 19. Dez 2021) $
#  \$Revision: 1662 $
#  \$Author: alfred $
#  \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K12_local.sh $
#  \$Id: K12_local.sh 1662 2021-12-19 19:48:43Z alfred $
# Namespaces für den Cluster
#
---
apiVersion: v1
kind: Namespace
metadata:
  labels:
    kubernetes.io/metadata.name: slainte
  name: slainte
spec:
  finalizers:
  - kubernetes
---
apiVersion: v1
kind: Namespace
metadata:
  labels:
    kubernetes.io/metadata.name: default
  name: default
spec:
  finalizers:
  - kubernetes
---
apiVersion: v1
kind: Namespace
metadata:
  labels:
    kubernetes.io/metadata.name: admin
  name: admin
spec:
  finalizers:
  - kubernetes
---
EOF
#
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/namespace-slainte.yaml'
#
cat <<EOF > ${wd}/ingress-service.yaml
#  \$Date: 2021-12-19 20:48:43 +0100 (So, 19. Dez 2021) $
#  \$Revision: 1662 $
#  \$Author: alfred $
#  \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K12_local.sh $
#  \$Id: K12_local.sh 1662 2021-12-19 19:48:43Z alfred $
# Definition des Ingress-Services
#
---
kind: Service
apiVersion: v1
metadata:
  name: ingress
  namespace: ingress
  labels:
    app.kubernetes.io/name: ingress
    app.kubernetes.io/part-of: ingress
  annotations:      
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/ssl-temporary-redirect: "false"
    nginx.ingress.kubernetes.io/secure-backends: "true"
    nginx.ingress.kubernetes.io/ssl-proxy-headers: "X-Forwarded-Proto: https"
    nginx.ingress.kubernetes.io/proxy-body-size: 0m
    nginx.ingress.kubernetes.io/proxy-buffering: "off"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
# https://github.com/nginxinc/kubernetes-ingress/tree/v1.12.0/examples/ssl-services
###############    nginx.ingress.kubernetes.io/ssl-services: "Server-Name-svc"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
spec:
  # https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip
  externalTrafficPolicy: Local
  type: LoadBalancer
  # loadBalancerIP is optional. MetalLB will automatically allocate an IP from its pool if not
  # specified. You can also specify one manually.
  loadBalancerIP: 192.168.0.210
  # Der Ingress-Controller nach aussen für den Router ist immer auf 210
  # Damit kann dieser Ingress-Controller auch in diversen /etc/hosts eingetragen werden z.b.
  # 192.168.0.210	k8s.slainte.at  
  selector:
    name: nginx-ingress-microk8s
  ports:
    - name: https
      port: 443
      targetPort: https
    - name: http
      port: 80
      targetPort: http
---
EOF
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/ingress-service.yaml'
#
# Adaptieren der Services
cat <<EOF > ${wd}/do_loadbalancer.sh
#!/bin/bash
#
#    \$Date: 2021-12-19 20:48:43 +0100 (So, 19. Dez 2021) $
#    \$Revision: 1662 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K12_local.sh $
#    \$Id: K12_local.sh 1662 2021-12-19 19:48:43Z alfred $
#
# Ändern des Services auf Loadbalancer
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Dashboard - 192.168.0.211
microk8s kubectl -n kube-system get service kubernetes-dashboard -o yaml > ${id}/kube-dash-svc.yaml
sed 's/ClusterIP/LoadBalancer/' ${id}/kube-dash-svc.yaml > ${id}/new-kube-dash-svc.yaml
microk8s kubectl apply -f ${id}/new-kube-dash-svc.yaml
# longhorn - 192.168.0.212
microk8s kubectl -n longhorn-system get service longhorn-frontend -o yaml > ${id}/longhorn-svc.yaml
sed 's/ClusterIP/LoadBalancer/' ${id}/longhorn-svc.yaml > ${id}/new-longhorn-svc.yaml
microk8s kubectl apply -f ${id}/new-longhorn-svc.yaml
EOF
#
chmod 755 ${wd}/do_loadbalancer.sh
ansible pc1 -m shell -a ${id}'/do_loadbalancer.sh ' > ${wd}'/do_loadbalancer.log'
#
cat <<EOF > ${wd}/do_storageclass.sh
#!/bin/bash
#
#    \$Date: 2021-12-19 20:48:43 +0100 (So, 19. Dez 2021) $
#    \$Revision: 1662 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K12_local.sh $
#    \$Id: K12_local.sh 1662 2021-12-19 19:48:43Z alfred $
#
# Ändern der Storage-class
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

# Storageclasse microk78s-hostpath
microk8s kubectl get sc microk8s-hostpath -o yaml > ${id}/hostpath.yaml
sed 's/is-default-class: "true"/is-default-class: "false"/' ${id}/hostpath.yaml > ${id}/new-hostpath.yaml
microk8s kubectl apply -f ${id}/new-hostpath.yaml
# Storageclasse longhorn
microk8s kubectl get sc longhorn -o yaml > ${id}longhorn.yaml
sed 's/is-default-class: "false"/is-default-class: "true"/' ${id}/longhorn.yaml > ${id}/new-longhorn.yaml
microk8s kubectl apply -f ${id}/new-longhorn.yaml
# Storageclasse openebs-jiva-csi-default
#microk8s kubectl get sc openebs-jiva-csi-default -o yaml > ${id}/openebs-jiva-csi-default.yaml
#sed 's/meta.helm.sh\/release-namespace: openebs/meta.helm.sh\/release-namespace: openebs\\n    storageclass.kubernetes.io\/is-default-class: "true"/' ${id}/openebs-jiva-csi-default.yaml > ${id}/new-openebs-jiva-csi-default.yaml
#microk8s kubectl apply -f ${id}/new-openebs-jiva-csi-default.yaml
microk8s kubectl get sc
#
EOF
chmod 755 ${wd}/do_storageclass.sh
#
ansible pc1 -m shell -a ${id}'/do_storageclass.sh ' > ${wd}'/do_storageclass.log'
#
ansible pc1 -m shell -a 'microk8s kubectl get sc'
#
ansible pc1 -m shell -a 'microk8s kubectl get services --all-namespaces'
#
