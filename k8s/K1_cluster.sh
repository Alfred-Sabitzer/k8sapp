#!/bin/bash
############################################################################################
#    $Date: 2021-07-11 09:59:42 +0200 (So, 11. Jul 2021) $
#    $Revision: 532 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K1_cluster.sh $
#    $Id: K1_cluster.sh 532 2021-07-11 07:59:42Z alfred $
#
# Händische Tasks - Verbinden der Nodes zu einem Cluster
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 532 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}

# Verbinden der Nodes
ansible pc1 -m shell -a 'microk8s add-node > '${id}'/pc2.txt'
ansible pc2 -m shell -a 'cat '${id}'/pc2.txt | grep -i " microk8s join " | /bin/bash '
ansible pc1 -m shell -a 'microk8s add-node > '${id}'/pc3.txt'
ansible pc3 -m shell -a 'cat '${id}'/pc3.txt | grep -i " microk8s join " | /bin/bash '
ansible pc1 -m shell -a 'microk8s add-node > '${id}'/pc4.txt'
ansible pc4 -m shell -a 'cat '${id}'/pc4.txt | grep -i " microk8s join " | /bin/bash '
ansible pc1 -m shell -a 'microk8s add-node > '${id}'/pc5.txt'
ansible pc5 -m shell -a 'cat '${id}'/pc5.txt | grep -i " microk8s join " | /bin/bash '
#

