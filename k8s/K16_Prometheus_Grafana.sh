#!/bin/bash
############################################################################################
#    $Date: 2021-12-19 20:56:27 +0100 (So, 19. Dez 2021) $
#    $Revision: 1663 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K16_Prometheus_Grafana.sh $
#    $Id: K16_Prometheus_Grafana.sh 1663 2021-12-19 19:56:27Z alfred $
#
# Setzen LoadBalancer für Prometheus und Grafana
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 1663 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
cat <<EOF > ${wd}/do_prometheus.sh
#!/bin/bash
#
#    \$Date: 2021-12-19 20:56:27 +0100 (So, 19. Dez 2021) $
#    \$Revision: 1663 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K16_Prometheus_Grafana.sh $
#    \$Id: K16_Prometheus_Grafana.sh 1663 2021-12-19 19:56:27Z alfred $
#
# Ändern des Services auf Loadbalancer Prometheus 192.168.0.215
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
microk8s kubectl -n monitoring get service prometheus-k8s -o yaml > ${id}/prometheus-k8s.yaml
sed 's/ClusterIP/LoadBalancer/' ${id}/prometheus-k8s.yaml > ${id}/new-prometheus-k8s.yaml
microk8s kubectl apply -f ${id}/new-prometheus-k8s.yaml
EOF
chmod 755 ${wd}/do_prometheus.sh
#
ansible pc1 -m shell -a ${id}'/do_prometheus.sh ' > ${wd}'/do_prometheus.log'
#
cat <<EOF > ${wd}/do_grafana.sh
#!/bin/bash
#
#    \$Date: 2021-12-19 20:56:27 +0100 (So, 19. Dez 2021) $
#    \$Revision: 1663 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K16_Prometheus_Grafana.sh $
#    \$Id: K16_Prometheus_Grafana.sh 1663 2021-12-19 19:56:27Z alfred $
#
# Ändern des Services auf Loadbalancer Grafana 192.168.0.216
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
microk8s kubectl -n monitoring get service grafana -o yaml > ${id}/grafana.yaml
sed 's/ClusterIP/LoadBalancer/' ${id}/grafana.yaml > ${id}/new-grafana.yaml
microk8s kubectl apply -f ${id}/new-grafana.yaml
EOF
chmod 755 ${wd}/do_grafana.sh
#
ansible pc1 -m shell -a ${id}'/do_grafana.sh ' > ${wd}'/do_grafana.log'
#

