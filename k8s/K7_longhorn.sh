#!/bin/bash
############################################################################################
#    $Date: 2021-10-22 00:09:19 +0200 (Fr, 22. Okt 2021) $
#    $Revision: 669 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K7_longhorn.sh $
#    $Id: K7_longhorn.sh 669 2021-10-21 22:09:19Z alfred $
#
# Schnell-Installation microk8s - Longhorn Vorbereitung
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 669 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}

#
# initialisieren der fstab - entfernen eventueller Versuche
cat <<EOF > ${wd}/fstab
LABEL=writable	/	 ext4	defaults	0 0
LABEL=system-boot       /boot/firmware  vfat    defaults        0       1
ansible@monitoring:/home/ansible/copy /opt/cluster fuse.sshfs rw,delay_connect,idmap=user,uid=0,gid=0,umask=0,allow_other,_netdev,workaround=rename 0 0
EOF
ansible pc -m shell -a 'sudo cp -f '${id}'/fstab /etc/fstab'
ansible pc -m shell -a 'sudo cat /etc/fstab'
ansible pc -m shell -a 'sudo shutdown -r now'
#
sleep 2m
#
ansible-playbook -v ${HOME}/check_hosts.yaml
## Jetzt kommt Longhorn
ansible pc -m shell -a 'sudo wipefs -a /dev/sda'
ansible pc -m shell -a 'sudo mkfs.ext4 /dev/sda'
ansible pc -m shell -a 'sudo rm -f -R /var/lib/longhorn' # Das ist der Default - Der ist für die microSD
ansible pc -m shell -a 'sudo mkdir /var/lib/longhorn' # Das ist der Default  - Der ist für die microSD
ansible pc -m shell -a 'sudo mount /dev/sda /var/data/longhorn'
ansible pc -m shell -a 'df -h | grep -i sda'
# die Verbindung über ssh muß händisch gemacht werden.
# Hinzufügen der SSD-Disks
ansible pc -m shell -a 'sudo printf $(sudo blkid -o export /dev/sda | grep UUID)" /var/data/longhorn       ext4    defaults        0       2" | sudo tee -a /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
ansible pc -m shell -a 'sudo apt-get update'
ansible pc -m shell -a 'sudo apt-get upgrade -y'
ansible pc -m shell -a 'sudo apt-get -y install open-iscsi'
ansible pc -m shell -a 'sudo systemctl enable iscsid'
ansible pc -m shell -a 'sudo systemctl start iscsid'
ansible pc -m shell -a 'sudo systemctl status iscsid'
# https://longhorn.io/docs/1.1.1/deploy/install/
ansible pc -m shell -a 'sudo apt-get install curl util-linux jq nfs-common -y '
#
ansible pc1 -m shell -a 'microk8s.helm3 repo add longhorn https://charts.longhorn.io'
ansible pc1 -m shell -a 'microk8s kubectl create namespace longhorn-system'
ansible pc1 -m shell -a 'microk8s.helm3 install longhorn longhorn/longhorn --namespace longhorn-system --set csi.kubeletRootDir="/var/snap/microk8s/common/var/lib/kubelet"'
ansible pc1 -m shell -a 'microk8s kubectl -n longhorn-system get pod'
#
