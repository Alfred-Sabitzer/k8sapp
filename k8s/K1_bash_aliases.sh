#!/bin/bash
############################################################################################
#
# Schnell-Installation bash-aliases für ansible und alfred
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Installation der Serviceaccounts
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
revision="latest"
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/${revision}"
id="/opt/cluster/${app}/${xd}/${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
cat <<EOF > ${wd}/bash_completion.sh
#!/bin/bash
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
microk8s.kubectl completion bash >/etc/bash_completion.d/kubectl
EOF
chmod 755 ${wd}/bash_completion.sh
#
ansible pc -m shell -a 'sudo '${id}'/bash_completion.sh'
#
cat <<EOF > ${wd}/.bash_aliases
#
#Shortcut um sich tipparbeit zu sparen
alias linkerd='microk8s.linkerd '
alias k='kubectl'
alias kall='microk8s kubectl get all --all-namespaces && microk8s kubectl get ingresses --all-namespaces && microk8s kubectl get endpoints --all-namespaces'
#Zeigt die logs der Ingress-Pods
alias klt='(kubectl get pods --all-namespaces) | grep -i nginx-ingress-microk8s-controller | while read a b c; do kubectl logs "\$b" -n ingress; done'
alias helm='microk8s.helm3'
EOF
ansible pc -m shell -a 'sudo cp -f '${id}'/.bash_aliases /home/alfred/.bash_aliases '
ansible pc -m shell -a 'sudo cp -f '${id}'/.bash_aliases /home/ansible/.bash_aliases '
ansible pc -m shell -a 'sudo chown alfred:alfred /home/alfred/.bash_aliases '
ansible pc -m shell -a 'sudo chmod 664 /home/alfred/.bash_aliases '
ansible pc -m shell -a 'sudo chown ansible:ansible /home/ansible/.bash_aliases '
ansible pc -m shell -a 'sudo chmod 664 /home/alfred/.bash_aliases '
#
ansible pc -m lineinfile -a 'dest=~/.bashrc state=present regexp=rpc_json line='\''complete -F __start_kubectl k'\'''
#
ansible pc -m shell -a 'sudo echo  "source <(linkerd completion bash)" | sudo tee -a ~/.bashrc'
#
ansible pc -m shell -a 'sudo cp -f /home/ansible/.bashrc /home/alfred/.bashrc '
ansible pc -m shell -a 'sudo chown alfred:alfred /home/alfred/.bashrc '
ansible pc -m shell -a 'sudo chmod 664 /home/alfred/.bashrc '
##
