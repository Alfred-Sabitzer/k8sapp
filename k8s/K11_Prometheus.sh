#!/bin/bash
############################################################################################
#    $Date: 2021-08-09 16:52:50 +0200 (Mo, 09. Aug 2021) $
#    $Revision: 568 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K11_Prometheus.sh $
#    $Id: K11_Prometheus.sh 568 2021-08-09 14:52:50Z alfred $
#
# Enablen Prometheus und Grafana für Logging und Monitoring
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 568 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
ansible pc1 -m shell -a 'microk8s enable prometheus  ' 
ansible pc -m shell -a 'microk8s status --wait-ready'

##


