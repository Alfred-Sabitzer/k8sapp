#!/bin/bash
############################################################################################
#    $Date: 2021-10-21 21:40:29 +0200 (Do, 21. Okt 2021) $
#    $Revision: 659 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K5_certmanager.sh $
#    $Id: K5_certmanager.sh 659 2021-10-21 19:40:29Z alfred $
#
# cert-manager
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Scripts in der richtigen Reihenfolge
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 659 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
cat <<EOF > ${wd}/install_certmanager.sh
#!/bin/bash
#
#    \$Date: 2021-10-21 21:40:29 +0200 (Do, 21. Okt 2021) $
#    \$Revision: 659 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K5_certmanager.sh $
#    \$Id: K5_certmanager.sh 659 2021-10-21 19:40:29Z alfred $
#
# Installation des cert-managers mit helm
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Scripts in der richtigen Reihenfolge
microk8s kubectl create namespace cert-manager
microk8s helm3 repo add jetstack https://charts.jetstack.io
microk8s helm3 repo update
microk8s helm3 install cert-manager jetstack/cert-manager \
  --namespace cert-manager --version v1.5.4 \
  --set installCRDs=true \
  --set ingressShim.defaultIssuerName=letsencrypt-production \
  --set ingressShim.defaultIssuerKind=ClusterIssuer \
  --set ingressShim.defaultIssuerGroup=cert-manager.io

#
#wget https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.yaml --output-document=${id}/cert-manager.yaml
#wget https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.crds.yaml --output-document=${id}/cert-manager.crds.yaml
#microk8s kubectl apply -f ${id}/*.yaml
#
sleep 1m
microk8s kubectl get pods --namespace cert-manager  
EOF
chmod 755 ${wd}/install_certmanager.sh
#
ansible pc1 -m shell -a ${id}'/install_certmanager.sh'
#
