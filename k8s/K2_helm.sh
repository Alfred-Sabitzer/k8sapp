#!/bin/bash
############################################################################################
#    $Date: 2021-07-19 15:05:42 +0200 (Mo, 19. Jul 2021) $
#    $Revision: 543 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K2_helm.sh $
#    $Id: K2_helm.sh 543 2021-07-19 13:05:42Z alfred $
#
# Schnell-Installation microk8s - helm3
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Sauber installierte Nodes, Verbundener Cluster
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 543 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'microk8s enable helm3'
#
