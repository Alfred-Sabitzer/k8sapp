#!/bin/bash
############################################################################################
#    $Date: 2021-10-20 20:24:57 +0200 (Mi, 20. Okt 2021) $
#    $Revision: 640 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K10_Dashboard.sh $
#    $Id: K10_Dashboard.sh 640 2021-10-20 18:24:57Z alfred $
#
# Enablen des Kubernetes Dashboards
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 640 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
ansible pc1 -m shell -a 'microk8s enable dashboard ' 
ansible pc -m shell -a 'microk8s status --wait-ready'
cat <<EOF > ${wd}/clusterrolebinding.yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user # Name des bindings
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin # Name der role
subjects:
  - kind: ServiceAccount
    name: admin-user         # Name des Serviceaccounts
    namespace: kube-system
---    
EOF
#
ansible pc1 -m shell -a 'microk8s kubectl apply -f '${id}'/clusterrolebinding.yaml'
#   

cat <<EOF > ${wd}/show_secret.sh
#!/bin/bash
#
#    \$Date: 2021-10-20 20:24:57 +0200 (Mi, 20. Okt 2021) $
#    \$Revision: 640 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K10_Dashboard.sh $
#    \$Id: K10_Dashboard.sh 640 2021-10-20 18:24:57Z alfred $
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Ausgabe des Tokens für den Login
token=\$(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
microk8s kubectl -n kube-system describe secret \$token
# Ausgabe des Tokens für den Login für den Serviceuser

microk8s kubectl -n kube-system get secret \$(microk8s kubectl -n kube-system get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
microk8s kubectl -n kube-system describe secret \$(microk8s kubectl -n kube-system get secret | grep admin-user | awk '{print \$1}')

#
EOF
chmod 755 ${wd}/show_secret.sh
#
ansible pc1 -m shell -a ${id}'/show_secret.sh ' > ${wd}'/show_secret.log'
##
