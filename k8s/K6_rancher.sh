#!/bin/bash
############################################################################################
#    $Date: 2021-07-11 09:57:09 +0200 (So, 11. Jul 2021) $
#    $Revision: 531 $
#    $Author: alfred $
#    $HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K6_rancher.sh $
#    $Id: K6_rancher.sh 531 2021-07-11 07:57:09Z alfred $
#
# Einspielen des Ranchers. Weiter Applikationen werden ab jetzt direkt über den Rancher gemacht.
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Definitionen für das Deployment
#
exit 0
###### Kein Rancher, da kein Nutzen
##
sname=$(basename "$0")
app="mikrok8s/install/${sname}"
pf=\$"Revision: "
sf=" "\$
fr="\$Revision: 531 $"
revision=${fr#*"$pf"}
revision=${revision%"$sf"*}
xd=(`date '+%Y-%m-%d'`)
wd="${HOME}/copy/${app}/${xd}/r${revision}"
id="/opt/cluster/${app}/${xd}/r${revision}"
rm -f -R ${wd}
mkdir -p ${wd}
#
cat <<EOF > ${wd}/install_rancher.sh
#!/bin/bash
#
#    \$Date: 2021-07-11 09:57:09 +0200 (So, 11. Jul 2021) $
#    \$Revision: 531 $
#    \$Author: alfred $
#    \$HeadURL: https://monitoring.slainte.at/svn/slainte/trunk/k8s/k8s/K6_rancher.sh $
#    \$Id: K6_rancher.sh 531 2021-07-11 07:57:09Z alfred $
#
# Installation des Ranchers-managers mit helm
#
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Voraussetzung: Scripts in der richtigen Reihenfolge
microk8s.kubectl create namespace cattle-system 
microk8s.kubectl label namespace cattle-system cattle-system.k8s.io/disable-validation=true 
microk8s enable helm3 
microk8s.helm3 repo add rancher-latest https://releases.rancher.com/server-charts/stable
microk8s.helm3 repo update 
microk8s.helm3 install rancher rancher-latest/rancher --namespace cattle-system  --set replicas=1 --set hostname=pc.home
sleep 1m
microk8s kubectl get pods --all-namespaces
EOF
chmod 755 ${wd}/install_rancher.sh
#
ansible pc1 -m shell -a ${id}'/install_rancher.sh  ' > ${wd}'/install_rancher.log'
#
##
