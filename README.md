# k8sApp

Beispielcode für das E-Book K8s Application

## K8S Application
Dieses Buchprojekt beschreibt die notwendigen Schritte um einen bare-metal-on-premise-kubernetes Cluster auf Raspberry-PI Basis zu bauen und zu betreiben. Es werden alle notwendigen Hilfswerkzeuge und Konfigurationen beschrieben. Dennoch ist dies keine Lehrbuch, sondern beschreibt einfach meine Erlebnisse auf dieser "Reise".

## Inhalte
Es gibt für jeden Teilabschnitt ein eigenes Unterprojekt. Diese Projekte sind an sich selbständig. Jedoch setzen alle einen bereits aufgesetzten und funktionierenden Kubernetes Cluster voraus. Auch für das Aufsetzen des Clusters gibt es einen Automatismus. Dieses Buch setzt auf den Vorarbeiten von ISBN 978-3-7531-9851-4 "Bau einer K8s bare-metal-cloud mit RaspberryPI" auf. Links sind zb. https://books.google.at/books/about/Bau_einer_K8s_bare_metal_cloud_mit_Raspb.html?id=aGhCEAAAQBAJ&redir_esc=y und https://www.thalia.de/shop/home/artikeldetails/A1062168670  

## Support
Es gibt keinen Support für diese Software. Jede Software hat experimentellen Charakter. Es wird dringend davon abgeraten, diese Software produktiv zu verwenden.

## Lizenz
GPL https://www.gnu.org/licenses/gpl-3.0.html
